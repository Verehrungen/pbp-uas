## Kelompok F01
## Anggota Kelompok:
- 2006484186	Rayhan Adhisworo Jardine H
- 2006486235	Wega Syafitra Winarso
- 2006464543	Juoro Algifari
- 2006486941	Atika Najla Febryani
- 2006464045	Indah Noviyanti
- 2006485112	Tsabita Sarah Indah Anggayasti
- 2006483706	Haura Rasya Maharani

## Link APK
https://drive.google.com/drive/u/2/folders/1wev2cXfKI6MUL3VGkjbuNV32NJS6on8a

## Cerita Aplikasi dan Manfaatnya
Aplikasi yang kami buat merupakan learning management system (LMS) yang berfungsi untuk membantu kegiatan belajar mengajar sekolah ataupun universitas apapun yang membutuhkan suatu platform pembelajaran terintegrasi secara daring. Aplikasi ini sangat berguna mengingat saat ini masih terjadi pandemi Covid-19, sehingga pembelajaran harus dilaksanakan jarak jauh. Aplikasi ini dapat diakses oleh siapapun. Dibutuhkan email/username dan password sebagai proses autentikasi ketika ingin mengakses aplikasi ini. Pada aplikasi ini terdapat landing page yang berisi informasi-informasi dan home page yang berisi informasi-informasi serta course yang telah diambil oleh mahasiswa. Setiap course akan mewakilkan suatu kelas atau mata kuliah. Pada course tersebut, mahasiswa dapat mengunduh materi yang telah diunggah oleh dosen, bertanya atau membagi pengetahuan, mengikuti kuis, dan mengumpulkan tugas.  

## Daftar Modul

- Authentication
    - Registration : Pengguna dapat mendaftarkan diri sebagai Murid atau Pengajar.
    - Login : Pengguna dapat masuk sebagai Murid atau Pengajar. Seorang Admin dapat login melalui page admin.
    - Ditampilkan informasi berbeda pada website sesuai peran pengguna.
    - Anonymous hanya dapat mengakses halaman utama, Helpdesk, dan ringkasan Course
- Forum diskusi
    - Berfungsi sebagai tempat berkomunikasi antar pengguna di suatu kelas. Dapat digunakan sebagai tempat berbagi ilmu, memberikan pertanyaan, melakukan pembicaraan terkait suatu Course.
    - Murid dan Pengajar dapat membuat post yang akan ditampilkan kepada semua orang yang ada di suatu Course
    - Murid dan Pengajar juga dapat memberikan reply atau komentar 
- Course management
    - Pusat dari proses belajar mengajar, berisi konten pembelajaran, forum diskusi, penilaian(submission).
    - Pengajar dapat membuat suatu Course, menambahkan konten, membuka forum diskusi, memberikan deskripsi.
    - Murid dapat mengenroll dan unenroll suatu Course.
- Submission/download
    - Modul untuk menyebarkan file ataupun mengunggah file.
    - Pengajar dapat memberikan tugas berupa submission kepada Murid. Tugas yang telah dikumpulkan nantinya dapat diberikan penilaian dan dapat didownload oleh Pengajar dan Murid.
    - Fitur ini dapat digunakan Pengajar untuk menyebarkan file yang mendukung proses pembelajaran.
- Dashboard profil
    - Menampilkan informasi-informasi penting suatu pengguna.
    - Pengguna dapat mengubah informasi yang ditampilkan (nama, foto profil, etc)
    - Murid dapat melihat Course apa saja yang telah dan sedang diambil, forum diskusi yang sedang diikuti, dan tugas yang telah dikumpulkan.
    - Pengajar dapat melihat tugas yang telah dikumpulkan, course yang telah dibuat, forum diskusi yang diikuti.
    - Admin dapat melihat semua Course yang ada di website. 
- Kuis
    - Pengajar dapat menambahkan, mengedit, serta menghapus soal, pertanyaan, dan jawaban.
    - Murid dapat melihat daftar kuis yang dibuat oleh pengajar.  
- Helpdesk
    - Form masukan atau komplain dari semua tipe pengguna (termasuk Anonymous) dan terdapat cara mengkontak Admin.
    - Admin dapat melihat masukan dari pengguna.


## Persona
4 Tipe Pengguna : Murid, Pengajar, Admin, Anonymous.

Contoh persona:
- Murid SMA
Goals:  
Toki adalah seorang murid SMA ternama di Depok. Toki perlu memiliki akun untuk mengikuti pembelajaran yang digunakan oleh guru Biologi di kelasnya. Selain itu, ia juga tidak memiliki pengalaman dalam menggunakan website sebagai media pembelajaran sehingga Toki merasa asing dalam penggunaan website tersebut. Oleh karena itu, Toki memerlukan website yang memberikan pengalaman intuitif serta user interface yang lebih sederhana sehingga lebih mudah dipahami.

Expectation: 
Akun untuk mengakses website
Pengalaman yang intuitif

Frustration: Kesulitan melakukan sign up, belum terbiasa mengakses website pembelajaran, dan user interface yang tidak rapi.


- Mahasiswa

Goals: 
Siesta adalah seorang mahasiswa S1 di suatu universitas di Surabaya. Siesta aktif mengikuti kegiatan-kegiatan yang ada di dalam maupun di luar universitasnya. Untuk mengikuti kegiatan pembelajaran jarak jauh di kampusnya seperti mengetahui informasi mengenai mata kuliah yang diambil, mengerjakan tugas yang diberikan dosen, dan berkomunikasi dengan dosen dan teman-temannya, Siesta harus membuka website dimana ia bisa melakukan hal-hal tersebut. Kesibukan Siesta ini membuat ia butuh mengakses informasi tersebut dengan cepat.

Expectation: 
Akses ke informasi yang cepat
Informasi yang detail mengenai mata kuliah yang diambil

Frustration:
User interface website yang tidak rapi dan alur mencari informasi yang tidak intuitif (sulit untuk dimengerti).


- Dosen

Goals:
Bu Sofita adalah seorang dosen mata kuliah Struktur Data dan Algorima di suatu universitas ternama di Jakarta. Karena COVID-19, Bu Sofita tidak bisa mengajar mahasiswanya secara langsung. Oleh karena itu, Bu Sofita membutuhkan website yang bisa menampung informasi mengenai mata kuliah yang ia ajar, sekaligus menjadi wadah untuk berdiskusi dengan mahasiswa/i yang mengambil mata kuliahnya. Bu Sofita seringkali perlu melakukan hal yang sama berulang kali, seperti menilai tugas para mahasiswa dengan efisien.

Expectation:
Fitur yang mendukung untuk melakukan diskusi dengan mahasiswa/i.

Frustration:
Website yang tidak responsif, navigasi website yang bertele-tele.


- Masyarakat Umum

Goals: Lisa merupakan seorang pekerja kantoran yang memiliki tetangga seorang guru Ekonomi di suatu SMA. Tetangganya sedang membutuhkan suatu aplikasi alternatif untuk melakukan pembelajaran secara daring. Lisa dapat melakukan rekomendasi kepada tetangganya dengan mereview fitur-fitur apa saja yang ada di website ini tanpa harus login.  Lisa dapat melihat website ini dan mereview semua ringkasan Course yang ada tanpa harus melakukan login. Lisa juga dapat memberikan masukan kepada para developer jika ditemukan kendala.



