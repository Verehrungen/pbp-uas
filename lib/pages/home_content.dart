import 'package:flutter/material.dart';
// import 'package:lorem_ipsum/lorem_ipsum.dart';
import 'dart:math';
import 'dart:convert';

import 'package:tkpbp/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:tkpbp/pages/auth/login.dart';
import 'package:tkpbp/pages/quiz/lecturer_quiz_screen.dart';
import 'package:tkpbp/pages/course/allcourses.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainContent extends StatefulWidget {
  List<User>? loggedInUser;
  MainContent({Key? key, required this.loggedInUser}) : super(key: key);

  @override
  State<MainContent> createState() => _MainContentState();
}

class _MainContentState extends State<MainContent> {
  List<User>? allUserLst;
  bool isLoading = true;

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await _getData();
    });
    super.initState();
  }

  Future<void> _getData() async {
    await getJsonData(context);
  }

  Future<void> getJsonData(BuildContext context) async {
    var response = await http.get(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/participants"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        allUserLst = userFromMap(response.body);
        print(allUserLst);
      });
      await saveAllUser();
      setState(() {
        isLoading = false;
      });
    } else {
      print("Error occured");
    }
  }

  Future<void> getAllUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("myData")) {
      var myData =
          json.decode(localData.getString("myData")!) as Map<String, dynamic>;
      allUserLst = myData['all_user'];
    }
  }

  Future<void> saveAllUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("myData")) {
      localData.remove("myData");
    }
    String encoded = userToMap(allUserLst!);

    var myData = json.encode({"all_user": encoded});
    localData.setString("myData", myData);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : Container(
            // color: Colors.grey,
            // padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
            child: Center(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(100, 40, 80, 40),
                    child: Text(
                      "Kelas Online Berkualitas",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 34),
                    ),
                  ),
                  Image.asset(
                    'assets/images/purp.png',
                    height: 200,
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AllCourses(
                                    loggedInUser: widget.loggedInUser,
                                  )));
                    },
                    icon: Icon(Icons.search),
                    label: Text('Lihat Semua Kelas'),
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(200, 50), primary: Colors.purple),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  ElevatedButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LecturerQuizScreen()));
                    },
                    icon: Icon(Icons.article),
                    label: Text('Ikut Quiz'),
                    style: ElevatedButton.styleFrom(
                        minimumSize: Size(200, 50), primary: Colors.blue),
                  ),
                ],
              ),
            ),
          );
  }
}
