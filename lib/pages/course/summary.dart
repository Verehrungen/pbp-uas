import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:tkpbp/models/course/course.dart';
import 'package:tkpbp/pages/course/course_detail.dart';
import 'package:tkpbp/models/course/course_post.dart';
import 'package:tkpbp/models/course/section.dart';
import 'package:tkpbp/models/course/summary_point.dart';
import 'package:tkpbp/models/user.dart';
import 'package:http/http.dart' as http;

class SummaryPoints extends StatefulWidget {
  SummaryPoints({Key? key, required this.course, required this.author})
      : super(key: key);

  Courses course;
  User author;

  @override
  _SummaryPointsState createState() => _SummaryPointsState();
}

class _SummaryPointsState extends State<SummaryPoints> {
  List<SummaryPoint>? summaryPoints;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _getData();
    });
  }

  Future<void> _getData() async {
    await getSummaryData(context);
  }

  Future<void> getSummaryData(BuildContext context) async {
    var response = await http.get(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/course/summary/${widget.course.pk}"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        summaryPoints = summaryPointFromJson(response.body);
        // = summaryPoints;
      });
    } else {
      print("Error occured");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Course Summary "),
      ),
      body: ListView(children: [
        Padding(
          padding: EdgeInsets.all(8),
          child: Card(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  Column(children: [
                    Text(
                      "${widget.course.fields!.title ?? "0"}",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Description:",
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.justify,
                    ),
                    Text(
                      "${widget.course.fields!.description ?? "0"}",
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.justify,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    dividerExp,
                    SizedBox(
                      height: 10,
                    ),
                    Text("Penulis",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 30,
                    ),
                    Text(
                        "${widget.author.fields!.firstName ?? widget.course.fields!.author}" +
                            " " +
                            "${widget.author.fields!.lastName}"),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    dividerExp,
                    SizedBox(
                      height: 10,
                    ),
                    Text("Subtitle",
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    SizedBox(
                      width: 30,
                    ),
                    Text("${widget.course.fields!.subtitle}"),
                    SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    dividerExp,
                  ]
                      // summaryPoints == null
                      //     ? [Text("Kosong")]
                      //     : summaryPoints!.map((e) => Text(e.fields!.title!)).toList(),
                      ),
                ],
              ),
            ),
          ),
        ),
        Card(
          elevation: 2,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  "!! Important Points !!",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: summaryPoints == null
                      ? [Text("Kosong")]
                      : summaryPoints!
                          .map(
                            (e) => ListTile(
                              title: Text(
                                "${e.fields!.title!}",
                                style: TextStyle(
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          )
                          .toList(),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
