import 'dart:convert';
import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:tkpbp/models/course/course.dart';
import 'package:tkpbp/models/course/course_post.dart';
import 'package:tkpbp/models/course/section.dart';
import 'package:tkpbp/models/user.dart';
import 'package:http/http.dart' as http;
import 'package:tkpbp/pages/forum/forum.dart';

class CourseDetail extends StatefulWidget {
  CourseDetail(
      {Key? key,
      required this.course,
      required this.author,
      required this.loggedInUser})
      : super(key: key);

  Courses course;
  User author;
  List<User>? loggedInUser;

  @override
  _CourseDetailState createState() => _CourseDetailState();
}

class _CourseDetailState extends State<CourseDetail> {
  bool isloading = true;
  var formVisible = false;
  var sectionsLst = <Section>[];
  var postLst = <CoursePost>[];
  var visibleLst = <bool>[for (int i = 0; i < 100; i++) false];
  var titleCtrlLst = <TextEditingController>[
    for (int i = 0; i < 100; i++) TextEditingController()
  ];
  var descCtrlLst = <TextEditingController>[
    for (int i = 0; i < 100; i++) TextEditingController()
  ];
  TextEditingController sect = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await _getData();
    });
  }

  Future<void> _getData() async {
    await getSectionData(context);

    if (sectionsLst.isNotEmpty) {
      for (var e in sectionsLst) {
        titleCtrlLst.add(TextEditingController());
        descCtrlLst.add(TextEditingController());
        visibleLst.add(false);
      }
      // print(visibleLst);
      for (var e in sectionsLst) {
        await getPostData(context, e.pk!);
        var temp = postLst;
        e.posts = temp;

        postLst = <CoursePost>[];
      }
    }

    setState(() {
      isloading = false;
    });
  }

  Future<void> getSectionData(BuildContext context) async {
    var response = await http.get(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/course/sections/${widget.course.pk}"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        sectionsLst = sectionFromMap(response.body);
        widget.course.sections.addAll(sectionsLst);
      });
    } else {
      print("Error occured");
    }
  }

  Future<void> postSectionData(BuildContext context) async {
    var response = await http.post(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/course/sections/add/${widget.course.pk}/"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        },
        body: jsonEncode({'title': sect.text}));
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        visibleLst.add(false);
        titleCtrlLst.add(TextEditingController());
        descCtrlLst.add(TextEditingController());
        print('success');
        sectionsLst = sectionFromMap(response.body);
        widget.course.sections = sectionsLst;
      });
    } else {
      print("Error occured");
    }
  }

  Future<void> getPostData(BuildContext context, int id) async {
    var response = await http.get(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/section/${id}"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        response.body.isEmpty
            ? postLst = <CoursePost>[]
            : postLst = coursePostFromMap(response.body);
      });
    } else {
      print("Error occured");
    }
  }

  Future<void> postCoursePostData(
      BuildContext context,
      Section sec,
      int sectionid,
      TextEditingController title,
      TextEditingController description) async {
    var response = await http.post(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/section/${widget.course.pk}/add/${sectionid}/"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        },
        body: jsonEncode({
          'title': title.text,
          'description': description.text,
        }));
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        response.body.isEmpty
            ? postLst = <CoursePost>[]
            : sec.posts = coursePostFromMap(response.body);
      });
    } else {
      print("Error occured");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Course Details'),
      ),
      body: SingleChildScrollView(
        child: isloading
            ? Center(child: CircularProgressIndicator())
            : Padding(
                padding: EdgeInsets.all(8),
                child: Card(
                    elevation: 4.0,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListTile(
                            title: Text(widget.course.fields!.title!,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20)),
                            subtitle: Text("Author: " +
                                widget.author.fields!.firstName.toString() +
                                " " +
                                widget.author.fields!.lastName.toString()),
                          ),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              widget.course.fields!.subtitle!,
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(8.0),
                            alignment: Alignment.centerLeft,
                            child: Text('Description:\n\n' +
                                widget.course.fields!.description!),
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.start,
                            children: [
                              TextButton(
                                child: const Text('ADD SECTION'),
                                onPressed: () {
                                  setState(() {
                                    formVisible = !formVisible;
                                  });
                                },
                              ),
                            ],
                          ),
                          Visibility(
                            visible: formVisible,
                            child: Container(
                                padding: EdgeInsets.all(16),
                                height: 200.0,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    TextField(
                                        controller: sect,
                                        decoration: InputDecoration(
                                          labelText: "Judul Section",
                                        )),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    FloatingActionButton(
                                        onPressed: () async {
                                          if (sect.text.length != 0) {
                                            // Ajax call to POST
                                            await postSectionData(context);

                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                                    content: Text(
                                                        "Added a new section"),
                                                    backgroundColor:
                                                        Colors.green));
                                          } else {
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(SnackBar(
                                                    content: Text(
                                                        "Fill in the title"),
                                                    backgroundColor:
                                                        Colors.red));
                                          }

                                          setState(() {
                                            formVisible = !formVisible;
                                          });
                                        },
                                        child: Icon(Icons.add))
                                  ],
                                )),
                          ),
                          dividerExp,
                          Container(
                            padding: EdgeInsets.all(16),
                            child: Text("Course Content",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 20)),
                          ),
                          Column(
                            // For every section
                            children: sectionsLst
                                .map((e) => Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      16.0, 16, 16, 2),
                                              child: Text(
                                                e.fields!.title!,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 17),
                                              ),
                                            ),
                                            TextButton(
                                              child: const Text('New Post+'),
                                              onPressed: () {
                                                setState(() {
                                                  visibleLst[sectionsLst
                                                          .indexOf(e)] =
                                                      !visibleLst[sectionsLst
                                                          .indexOf(e)];
                                                });
                                              },
                                            ),
                                          ],
                                        ),

                                        dividerExp,
                                        Visibility(
                                          visible: visibleLst[
                                              sectionsLst.indexOf(e)],
                                          child: Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  8, 0, 8, 0),
                                              height: 200.0,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  TextField(
                                                      controller: titleCtrlLst[
                                                          sectionsLst
                                                              .indexOf(e)],
                                                      decoration:
                                                          InputDecoration(
                                                        labelText: "Judul Post",
                                                      )),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  TextField(
                                                      controller: descCtrlLst[
                                                          sectionsLst
                                                              .indexOf(e)],
                                                      decoration:
                                                          InputDecoration(
                                                        labelText: "Deskripsi",
                                                      )),
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  FloatingActionButton(
                                                      mini: true,
                                                      onPressed: () async {
                                                        if (titleCtrlLst[sectionsLst
                                                                        .indexOf(
                                                                            e)]
                                                                    .text
                                                                    .length !=
                                                                0 &&
                                                            descCtrlLst[sectionsLst
                                                                        .indexOf(
                                                                            e)]
                                                                    .text
                                                                    .length !=
                                                                0) {
                                                          // Ajax call to POST
                                                          await postCoursePostData(
                                                              context,
                                                              e,
                                                              e.pk!,
                                                              titleCtrlLst[
                                                                  sectionsLst
                                                                      .indexOf(
                                                                          e)],
                                                              descCtrlLst[
                                                                  sectionsLst
                                                                      .indexOf(
                                                                          e)]);

                                                          ScaffoldMessenger.of(
                                                                  context)
                                                              .showSnackBar(SnackBar(
                                                                  content: Text(
                                                                      "Added a new post"),
                                                                  backgroundColor:
                                                                      Colors
                                                                          .green));
                                                        } else {
                                                          ScaffoldMessenger.of(
                                                                  context)
                                                              .showSnackBar(SnackBar(
                                                                  content: Text(
                                                                      "Fill in the title & description"),
                                                                  backgroundColor:
                                                                      Colors
                                                                          .red));
                                                        }

                                                        setState(() {
                                                          visibleLst[sectionsLst
                                                                  .indexOf(e)] =
                                                              !visibleLst[
                                                                  sectionsLst
                                                                      .indexOf(
                                                                          e)];
                                                        });
                                                      },
                                                      child: Icon(Icons.add))
                                                ],
                                              )),
                                        ),
                                        // For every post in the section
                                        ExpansionPanelList(
                                          expansionCallback:
                                              (int index, bool isExpanded) {
                                            setState(() {
                                              e.posts[index].isExpanded =
                                                  !isExpanded;
                                            });
                                          },
                                          children: e.posts.isEmpty
                                              ? [
                                                  ExpansionPanel(
                                                      headerBuilder: (context,
                                                          isExpanded) {
                                                        return ListTile(
                                                            title:
                                                                Text("Kosong"));
                                                      },
                                                      body: ListTile(
                                                          title:
                                                              Text("Kosong")),
                                                      isExpanded: false)
                                                ]
                                              : e.posts
                                                  .map((ef) => ExpansionPanel(
                                                      headerBuilder: (context,
                                                          isExpanded) {
                                                        return ListTile(
                                                            title: Text(ef
                                                                .fields!
                                                                .title!));
                                                      },
                                                      body: Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(16.0),
                                                          child: Text(ef.fields!
                                                              .description!)),
                                                      isExpanded:
                                                          ef.isExpanded))
                                                  .toList(),
                                        ),
                                      ],
                                    ))
                                .toList(),
                          ),
                          dividerExp,
                          // Container(
                          //   padding: EdgeInsets.all(16),
                          //   child: TextButton(
                          //     child: Text("Kuis/Ujian",
                          //         style: TextStyle(
                          //             fontWeight: FontWeight.bold, fontSize: 20)),
                          //     onPressed: () => {},
                          //   ),
                          // ),
                          // dividerExp,
                          // Container(
                          //   padding: EdgeInsets.all(16),
                          //   child: TextButton(
                          //     child: Text("Files",
                          //         style: TextStyle(
                          //             fontWeight: FontWeight.bold, fontSize: 20)),
                          //     onPressed: () => {},
                          //   ),
                          // ),
                          // dividerExp,
                          // Container(
                          //   padding: EdgeInsets.all(16),
                          //   child: TextButton(
                          //     child: Text("Forum Diskusi",
                          //         style: TextStyle(
                          //             fontWeight: FontWeight.bold, fontSize: 20)),
                          //     onPressed: () => {},
                          //   ),
                          // ),
                        ],
                      ),
                    ))),
      ),
    );
  }
}

var dividerExp = Padding(
  padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
  child: Row(children: <Widget>[
    Expanded(
        child: Divider(
      thickness: 2,
    )),
  ]),
);
