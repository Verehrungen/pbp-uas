import 'package:flutter/material.dart';
import 'package:tkpbp/pages/course/course_detail.dart';
import 'package:tkpbp/pages/course/summary.dart';
import 'package:tkpbp/pages/forum/forum.dart';
import 'package:tkpbp/widgets/main_drawer.dart';
import 'package:tkpbp/models/course/course.dart';
import 'package:tkpbp/models/user.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AllCourses extends StatefulWidget {
  List<User>? loggedInUser;
  AllCourses({Key? key, required this.loggedInUser}) : super(key: key);

  @override
  State<AllCourses> createState() => _AllCoursesState();
}

class _AllCoursesState extends State<AllCourses> {
  bool isloading = true;
  var listCrs = <Courses>[];
  var allUsrs = <User>[];
  var authors = <User>[];
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await getAllUser();
      await _getData();
      setState(() {
        isloading = false;
      });
    });

    super.initState();
  }

  void getAuthor() {
    for (var crs in listCrs) {
      for (var usr in allUsrs) {
        if (crs.fields!.author == usr.pk) {
          setState(() {
            authors.add(usr);
          });
        }
      }
    }
  }

  Future<void> _getData() async {
    await getJsonData(context);
    getAuthor();
  }

  Future<void> getJsonData(BuildContext context) async {
    var response = await http.get(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/all-courses/"),
        headers: {
          "Accept": "aplication/json",
        });
    // print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        listCrs = coursesFromJson(response.body);
      });
    } else {
      print("Error occured");
    }
  }

  Future<void> getAllUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("myData")) {
      var myData =
          json.decode(localData.getString("myData")!) as Map<String, dynamic>;
      allUsrs = userFromMap(myData['all_user']!.toString());
      print(allUsrs);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("All Courses"),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back))
        ],
      ),
      drawer: MainDrawer(loggedInUser: widget.loggedInUser),
      body: isloading
          ? Center(child: CircularProgressIndicator())
          : Padding(
              padding: EdgeInsets.all(8),
              child: ListView.builder(
                  itemCount: listCrs.length,
                  itemBuilder: (context, index) {
                    return Card(
                      elevation: 10,
                      child: Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "${listCrs[index].fields!.title ?? "0"}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 22),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Text(
                              "Author: ${authors[index].fields!.firstName ?? listCrs[index].fields!.author}" +
                                  " " +
                                  "${authors[index].fields!.lastName}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Text(
                              "Subtitle: ${listCrs[index].fields!.subtitle ?? "0"}",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14),
                            ),
                            SizedBox(
                              height: 14,
                            ),
                            Text(
                              "Description: ${listCrs[index].fields!.description ?? "0"}",
                              style: TextStyle(fontSize: 14),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(children: [
                              ElevatedButton.icon(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => CourseDetail(
                                                course: listCrs[index],
                                                author: authors[index],
                                                loggedInUser:
                                                    widget.loggedInUser,
                                              )));
                                },
                                icon: Icon(Icons.info),
                                label: Text("Details"),
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(125, 40)),
                              ),
                              SizedBox(width: 20),
                              ElevatedButton.icon(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => SummaryPoints(
                                              course: listCrs[index],
                                              author: authors[index])));
                                },
                                icon: Icon(Icons.library_books),
                                label: Text("Read Summary"),
                                style: ElevatedButton.styleFrom(
                                    minimumSize: Size(125, 40)),
                              ),
                            ]),
                            ElevatedButton.icon(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Forum(
                                              course: listCrs[index],
                                              loggedInUser: widget.loggedInUser,
                                            )));
                              },
                              icon: Icon(Icons.library_books),
                              label: Text("Forum Diskusi"),
                              style: ElevatedButton.styleFrom(
                                  minimumSize: Size(125, 40)),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
    );
  }
}
