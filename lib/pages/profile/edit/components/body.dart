import 'package:flutter/material.dart';
import 'package:tkpbp/pages/home.dart';
import 'package:tkpbp/pages/profile/edit/components/background.dart';
import 'package:tkpbp/pages/profile/edit/components/profile.dart';
import 'package:tkpbp/pages/profile/show/welcome_screen.dart';
import 'package:tkpbp/models/user.dart';
import 'package:tkpbp/widgets/components/rounded_button.dart';
import 'package:tkpbp/widgets/components/rounded_input_name.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Body extends StatefulWidget {
  Body({Key? key, required this.loggedIn}) : super(key: key);

  List<User>? loggedIn;

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<User>? resData;
  String email = "";
  String firstName = "";
  String lastName = "";
  String hobby = "";
  String motto = "";

  Future<void> postUpdateProfileData(BuildContext context) async {
    var response = await http.post(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/update_user/${widget.loggedIn![0].pk}/"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        },
        body: jsonEncode({
          'email': email,
          'nama_depan': firstName,
          'nama_belakang': lastName,
          'hobby': hobby,
          'motto': motto,
        }));
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        resData = userFromMap(response.body);
        // registeredUser = userFromMap(response.body);
        // sectionsLst = sectionFromMap(response.body);
        // widget.course.sections = sectionsLst;
      });
    } else {
      print("Error occured");
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "PROFILE",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.02),
            ProfilePic(),
            RoundedInputField(
              hintText: "First Name",
              onChanged: (value) {
                firstName = value;
              },
              icon: Icons.person,
            ),
            RoundedInputField(
              hintText: "Last Name",
              onChanged: (value) {
                lastName = value;
              },
              icon: Icons.person,
            ),
            RoundedInputField(
              hintText: "Hobby",
              onChanged: (value) {
                hobby = value;
              },
              icon: Icons.sports_baseball,
            ),
            RoundedInputField(
              hintText: "Motto",
              onChanged: (value) {
                motto = value;
              },
              icon: Icons.format_quote,
            ),
            RoundedInputField(
              hintText: "Email",
              onChanged: (value) {
                email = value;
              },
              icon: Icons.email,
            ),
            RoundedButton(
              text: "UPDATE",
              press: () async {
                try {
                  await postUpdateProfileData(context);

                  if (resData != null) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Changed Profile"),
                        backgroundColor: Colors.green));
                    setState(() {
                      widget.loggedIn = resData;
                    });
                    // Navigator.pop(context);
                    // Navigator.pop(context);
                    // Navigator.pop(context);

                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return HomeLab7(
                            loggedInUser: widget.loggedIn,
                          );
                        },
                      ),
                    );
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Failed to Update Profile"),
                        backgroundColor: Colors.red));
                  }
                } catch (e) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Failed to Update Profile"),
                      backgroundColor: Colors.red));
                }

                // firstName.isEmpty
                //     ? null
                //     : widget.loggedIn![0].fields!.firstName = firstName;
                // lastName.isEmpty
                //     ? null
                //     : widget.loggedIn![0].fields!.lastName = lastName;
                // hobby.isEmpty
                //     ? null
                //     : widget.loggedIn![0].fields!.hobby = hobby;
                // motto.isEmpty
                //     ? null
                //     : widget.loggedIn![0].fields!.motto = motto;
                // email.isEmpty
                //     ? null
                //     : widget.loggedIn![0].fields!.email = email;
                // print(widget.loggedIn![0].toMap());
                // print(firstName);
                // print(lastName);
                // print(hobby);
                // print(motto);
                // print(email);
              },
            ),
            SizedBox(height: size.height * 0.03),
          ],
        ),
      ),
    );
  }
}
