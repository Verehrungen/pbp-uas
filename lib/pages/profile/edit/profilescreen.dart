import 'package:flutter/material.dart';
import 'package:tkpbp/pages/profile/edit/components/body.dart';
import 'package:tkpbp/models/user.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key, required this.loggedIn}) : super(key: key);
  List<User>? loggedIn;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Body(
        loggedIn: loggedIn,
      ),
    );
  }
}
