import 'package:flutter/material.dart';
import 'package:tkpbp/models/user.dart';
import 'components/body.dart';

class WelcomeScreen extends StatelessWidget {
  List<User>? loggedIn;
  WelcomeScreen({Key? key, required this.loggedIn}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Body(
        loggedIn: loggedIn,
      ),
    );
  }
}
