import 'package:flutter/material.dart';
import 'package:tkpbp/pages/profile/edit/profilescreen.dart';
import 'package:tkpbp/models/user.dart';
import 'package:tkpbp/pages/profile/show/components/profileupdate.dart';
import 'package:tkpbp/widgets/components/rounded_button.dart';
import 'package:tkpbp/widgets/components/rounded_input_name.dart';

import 'background.dart';

class Body extends StatelessWidget {
  List<User>? loggedIn;

  Body({Key? key, required this.loggedIn}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "YOUR PROFILE",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            SizedBox(height: size.height * 0.03),
            ProfilePicture(),
            SizedBox(height: size.height * 0.03),
            RoundedInputFieldDisabled(
              hintText: "First Name",
              data: loggedIn![0].fields!.firstName ?? "Kosong",
              onChanged: (value) {},
              icon: Icons.person,
            ),
            RoundedInputFieldDisabled(
              hintText: "Last Name",
              data: loggedIn![0].fields!.lastName ?? "Kosong",
              onChanged: (value) {},
              icon: Icons.person,
            ),
            RoundedInputFieldDisabled(
              hintText: "Hobby Name",
              data: loggedIn![0].fields!.hobby!.isEmpty
                  ? "Kosong"
                  : loggedIn![0].fields!.hobby!,
              onChanged: (value) {},
              icon: Icons.sports_baseball,
            ),
            RoundedInputFieldDisabled(
              hintText: "Motto",
              data: loggedIn![0].fields!.motto!.isEmpty
                  ? "Kosong"
                  : loggedIn![0].fields!.motto!,
              onChanged: (value) {},
              icon: Icons.format_quote,
            ),
            RoundedInputFieldDisabled(
              hintText: "Email",
              data: loggedIn![0].fields!.email!.isEmpty
                  ? "Kosong"
                  : loggedIn![0].fields!.email,
              onChanged: (value) {},
              icon: Icons.email,
            ),
            RoundedButton(
              text: "EDIT",
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return ProfileScreen(
                        loggedIn: loggedIn,
                      );
                    },
                  ),
                );
              },
            ),
            SizedBox(height: size.height * 0.03),
          ],
        ),
      ),
    );
  }
}
