import 'package:flutter/material.dart';
import 'package:tkpbp/pages/home_content.dart';
import 'package:tkpbp/models/user.dart';
import 'package:tkpbp/widgets/main_drawer.dart';

class HomeLab7 extends StatelessWidget {
  List<User>? loggedInUser;
  HomeLab7({Key? key, required this.loggedInUser}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Aplikasi Belajar'),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      drawer: MainDrawer(
        loggedInUser: loggedInUser,
      ),
      body: SingleChildScrollView(
        child: MainContent(
          loggedInUser: loggedInUser,
        ),
      ),
    );
  }
}
