import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/pages/quiz/display.dart';
import 'package:tkpbp/models/global_state.dart';

class StudentScreen extends StatefulWidget {
  @override
  _StudentScreenState createState() => _StudentScreenState();
}

class _StudentScreenState extends State<StudentScreen> {
  @override
  void didChangeDependencies() {
    Provider.of<GState>(context, listen: false).getQuizzesData();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final quizzesData = Provider.of<GState>(context).quizzes;
    // print(quizzesData);
    return Scaffold(
      appBar: AppBar(
        title: Text('Quiz List'),
      ),
      body: quizzesData.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'No Quiz to Display',
                  ),
                ],
              ),
            )
          : Padding(
              padding: EdgeInsets.all(12),
              child: ListView.builder(
                itemCount: quizzesData.length,
                itemBuilder: (context, index) {
                  var item = quizzesData[index];
                  return Card(
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: InkWell(
                      hoverColor: Colors.black12,
                      child: Container(
                        padding: EdgeInsets.all(12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "${item.name} - ${item.topic}",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 4),
                                Text(
                                  "Time Limit: ${item.time} minutes \nNumber of Questions: ${item.numberOfQuestion} \nRequired Score to Pass: ${item.requiredScoreToPass}",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        DisplayScreen(id: item.id)));
                              },
                              icon: Icon(Icons.play_arrow_outlined),
                              tooltip: "Click to Attempt",
                              color: Colors.purple,
                            )
                          ],
                        ),
                      ),
                      // onTap: () {
                      //   print(item.id);
                      //   Navigator.of(context).push(MaterialPageRoute(
                      //       builder: (context) => DisplayScreen(id: item.id)));
                      // },
                    ),
                  );
                },
              ),
            ),
    );
  }
}
