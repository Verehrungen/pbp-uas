import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/models/global_state.dart';

class DisplayScreen extends StatefulWidget {
  final int id;
  DisplayScreen({required this.id});

  @override
  State<StatefulWidget> createState() {
    return DisplayScreenState();
  }
}

class DisplayScreenState extends State<DisplayScreen> {
  @override
  void didChangeDependencies() {
    Provider.of<GState>(context, listen: false).getQuizzesData();
    Provider.of<GState>(context).getQuestionData();
    Provider.of<GState>(context).getQuestionOfQuiz(widget.id);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final quizData = Provider.of<GState>(context).singleQuiz(widget.id);
    final questionsData = Provider.of<GState>(context).questionOfQuiz;
    List<CompleteQuestion> completeQuestion = [];

    questionsData.forEach((element) {
      int answerIndex = 0;
      Provider.of<GState>(context).getAnswerOfQuestion(element.id);
      List<Answer> answers = Provider.of<GState>(context).answerOfQuestion;

      CompleteQuestion c = CompleteQuestion(
          id: element.id,
          text: element.text,
          quizId: element.quizId,
          options: answers,
          answerIndex: answerIndex);
      completeQuestion.add(c);
    });

    return Scaffold(
      appBar: AppBar(
        title: Text(quizData.topic),
      ),
      body: completeQuestion.isEmpty
          ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'No Question to Display',
                  ),
                ],
              ),
            )
          : Padding(
              padding: EdgeInsets.all(12),
              child: ListView.builder(
                itemCount: questionsData.length,
                itemBuilder: (context, index) {
                  var item = completeQuestion[index];
                  return Container(
                    padding: EdgeInsets.all(12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              item.text,
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(height: 4),
                            Text(
                              item.options.length.toString(),
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
    );
  }
}
