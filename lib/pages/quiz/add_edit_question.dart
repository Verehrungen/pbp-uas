import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/models/global_state.dart';

class AddEditQuestionScreen extends StatefulWidget {
  static const routeName = '/add-edit-question';
  final int? id;
  AddEditQuestionScreen([this.id]);

  @override
  State<StatefulWidget> createState() {
    return _AddEditQuestionScreenState();
  }
}

class _AddEditQuestionScreenState extends State<AddEditQuestionScreen> {
  String? _text;
  int? _quiz;
  late List<Quiz> quiz;
  late Question prev;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  final textController = TextEditingController();

  @override
  void initState() {
    Provider.of<GState>(context, listen: false).getQuizzesData();
    if (widget.id != null) {
      prev = Provider.of<GState>(context, listen: false)
          .singleQuestion(widget.id!);
      textController.text = prev.text;
      _quiz = prev.quizId;
    }
    super.initState();
  }

  void _addQuiz() {
    if (!_form.currentState!.validate()) {
      return;
    }

    _form.currentState!.save();
    print(_text);
    print(_quiz);

    if (widget.id != null) {
      Provider.of<GState>(context, listen: false)
          .updateQuestion(widget.id!, _text!, _quiz!);
    } else {
      Provider.of<GState>(context, listen: false)
          .addNewQuestion(_text!, _quiz!);
    }
    Navigator.of(context).pop();
  }

  Widget _buildTextField() {
    return TextFormField(
      controller: textController,
      maxLines: 10,
      decoration: InputDecoration(
        labelText: 'Text',
        helperText: "Question's Text",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
        suffixIcon: textController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => textController.clear(),
              ),
      ),
      validator: (String? val) {
        if (val!.isEmpty) {
          return 'Text is required';
        }
        return null;
      },
      onSaved: (String? val) {
        _text = val!;
      },
    );
  }

  Widget _buildQuizField() {
    return DropdownButtonFormField<int>(
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
      ),
      value: widget.id != null ? prev.quizId : quiz[0].id,
      onChanged: (value) {
        setState(() {
          _quiz = value!;
        });
      },
      items: quiz.isNotEmpty
          ? quiz.map((aquiz) {
              return DropdownMenuItem(
                  value: aquiz.id,
                  child: Text('${aquiz.name} - ${aquiz.topic}'));
            }).toList()
          : null,
      validator: (val) {
        if (_quiz == null) return "Quiz is required";
        return null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    quiz = Provider.of<GState>(context).quizzes;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.id != null ? 'Edit Question' : 'Add Question'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ListView(
                shrinkWrap: true,
                children: [
                  Form(
                    key: _form,
                    child: Column(
                      children: <Widget>[
                        _buildTextField(),
                        SizedBox(
                          height: 6,
                        ),
                        _buildQuizField(),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 24),
                  color: Colors.purple,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                  ),
                  child: Text(
                    widget.id != null ? 'Edit Question' : 'Add Question',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onPressed: () {
                    _addQuiz();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
