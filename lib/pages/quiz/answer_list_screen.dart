import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/models/global_state.dart';
import 'package:tkpbp/pages/quiz/add_edit_answer.dart';
import 'package:tkpbp/models/global_state.dart';

class DetailQuestionScreen extends StatefulWidget {
  final int id;
  DetailQuestionScreen({required this.id});

  static const routeName = '/detail-question';
  @override
  _DetailQuestionScreenState createState() => _DetailQuestionScreenState();
}

class _DetailQuestionScreenState extends State<DetailQuestionScreen> {
  @override
  void didChangeDependencies() {
    Provider.of<GState>(context).getAnswerData();
    Provider.of<GState>(context).getAnswerOfQuestion(widget.id);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final answersData = Provider.of<GState>(context).answerOfQuestion;
    return Scaffold(
        appBar: AppBar(
          title: Text('List of Answers'),
        ),
        body: answersData.isEmpty
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'No Answer to Display',
                    ),
                  ],
                ),
              )
            : Padding(
                padding: EdgeInsets.all(12),
                child: ListView.builder(
                  itemCount: answersData.length,
                  itemBuilder: (context, index) {
                    var item = answersData[index];
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  item.text,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 5),
                                Text(
                                  "Correct Answer: ${item.correct}",
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        AddEditAnswerScreen(item.id)));
                              },
                              icon: Icon(Icons.edit),
                              color: Colors.green,
                            ),
                            IconButton(
                              onPressed: () {
                                Provider.of<GState>(context, listen: false)
                                    .deleteAnswer(item.id);
                              },
                              icon: Icon(Icons.delete),
                              color: Colors.red,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
        floatingActionButton: FloatingActionButton.extended(
          icon: Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => AddEditAnswerScreen()));
          },
          label: Text('Add Answer'),
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
        ));
  }
}
