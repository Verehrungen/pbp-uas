import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/models/global_state.dart';

class AddEditAnswerScreen extends StatefulWidget {
  static const routeName = '/add-edit-answer';
  final int? id;
  AddEditAnswerScreen([this.id]);

  @override
  State<StatefulWidget> createState() {
    return _AddEditAnswerScreenState();
  }
}

class _AddEditAnswerScreenState extends State<AddEditAnswerScreen> {
  String? _text;
  bool _correct = false;
  int? _question;
  late List<Question> questions;
  late Answer prev;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  final textController = TextEditingController();

  @override
  void initState() {
    Provider.of<GState>(context, listen: false).getQuestionData();
    if (widget.id != null) {
      prev =
          Provider.of<GState>(context, listen: false).singleAnswer(widget.id!);
      textController.text = prev.text;
      _correct = prev.correct;
      _question = prev.questionId;
    }
    super.initState();
  }

  void _addAnswer() {
    if (!_form.currentState!.validate()) {
      return;
    }

    _form.currentState!.save();
    if (widget.id != null) {
      Provider.of<GState>(context, listen: false)
          .updateAnswer(widget.id!, _text!, _correct, _question!);
    } else {
      Provider.of<GState>(context, listen: false)
          .addNewAnswer(_text!, _correct, _question!);
    }
    Navigator.of(context).pop();
  }

  Widget _buildTextField() {
    return TextFormField(
      controller: textController,
      maxLines: 10,
      decoration: InputDecoration(
        labelText: 'Text',
        helperText: "Answer's Text",
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
        suffixIcon: textController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => textController.clear(),
              ),
      ),
      validator: (String? val) {
        if (val!.isEmpty) {
          return 'Text is required';
        }
        return null;
      },
      onSaved: (String? val) {
        _text = val!;
      },
    );
  }

  Widget _buidCorrectField() {
    return SizedBox(
      height: 85,
      child: Padding(
        padding: EdgeInsets.only(bottom: 6.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(10),
          ),
          child: CheckboxListTile(
            title: Text('Correctness'),
            subtitle: Text('Is this the correct answer?'),
            secondary: Icon(Icons.check),
            checkColor: Colors.white,
            controlAffinity: ListTileControlAffinity.trailing,
            value: _correct,
            selected: _correct,
            onChanged: (bool? val) {
              setState(() {
                _correct = val!;
              });
            },
          ),
        ),
      ),
    );
  }

  Widget _questionField() {
    return DropdownButtonFormField<int>(
      decoration: InputDecoration(
        helperText: 'Answer of which question?',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
      ),
      value: widget.id != null ? prev.questionId : questions[0].id,
      onChanged: (value) {
        setState(() {
          _question = value;
        });
      },
      items: questions.isNotEmpty
          ? questions.map((q) {
              return DropdownMenuItem(value: q.id, child: Text('${q.text}'));
            }).toList()
          : null,
      validator: (int? val) {
        if (_question == null) return "Question is required";
        return null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    questions = Provider.of<GState>(context).questions;
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.id != null ? 'Edit Answer' : 'Add Answer'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ListView(
                shrinkWrap: true,
                children: [
                  Form(
                    key: _form,
                    child: Column(
                      children: <Widget>[
                        _buildTextField(),
                        SizedBox(height: 7),
                        _buidCorrectField(),
                        _questionField(),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(vertical: 20, horizontal: 24),
                  color: Colors.purple,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  child: Text(
                    widget.id != null ? 'Edit Answer' : 'Add Answer',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                    ),
                  ),
                  onPressed: () {
                    _addAnswer();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
