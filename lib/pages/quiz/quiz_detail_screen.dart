import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/pages/quiz/add_edit_question.dart';
import 'package:tkpbp/pages/quiz/answer_list_screen.dart';
import 'package:tkpbp/models/global_state.dart';

class DetailQuizScreen extends StatefulWidget {
  final int id;
  DetailQuizScreen({required this.id});

  static const routeName = '/detail-quiz';
  @override
  _DetailQuizScreenState createState() => _DetailQuizScreenState();
}

class _DetailQuizScreenState extends State<DetailQuizScreen> {
  @override
  void didChangeDependencies() {
    Provider.of<GState>(context).getQuestionData();
    Provider.of<GState>(context).getQuestionOfQuiz(widget.id);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final questionsData = Provider.of<GState>(context).questionOfQuiz;
    return Scaffold(
        appBar: AppBar(
          title: Text('List of Questions'),
        ),
        body: questionsData.isEmpty
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'No Question to Display',
                    ),
                  ],
                ),
              )
            : Padding(
                padding: EdgeInsets.all(12),
                child: ListView.builder(
                  itemCount: questionsData.length,
                  itemBuilder: (context, index) {
                    var item = questionsData[index];
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Container(
                        padding: EdgeInsets.all(12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  item.text,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(height: 13),
                                RaisedButton(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 13, horizontal: 30),
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                DetailQuestionScreen(
                                                    id: item.id)));
                                  },
                                  color: Colors.purple,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(16)),
                                  child: Text(
                                    "View Answers",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ],
                            ),
                            Spacer(),
                            IconButton(
                              onPressed: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        AddEditQuestionScreen(item.id)));
                              },
                              icon: Icon(Icons.edit),
                              color: Colors.green,
                            ),
                            IconButton(
                              onPressed: () {
                                Provider.of<GState>(context, listen: false)
                                    .deleteQuestion(item.id);
                              },
                              icon: Icon(Icons.delete),
                              color: Colors.red,
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
        floatingActionButton: FloatingActionButton.extended(
          icon: Icon(Icons.add),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => AddEditQuestionScreen()));
          },
          label: Text('Add Question'),
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
        ));
  }
}
