import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/models/global_state.dart';

class AddEditScreen extends StatefulWidget {
  static const routeName = '/add-edit-quiz';
  final int? id;
  AddEditScreen([this.id]);

  @override
  State<StatefulWidget> createState() {
    return _AddEditScreenState();
  }
}

class _AddEditScreenState extends State<AddEditScreen> {
  late String _name;
  late String _topic;
  late int _number_of_question;
  late int _time;
  late int _required_score;

  final GlobalKey<FormState> _form = GlobalKey<FormState>();

  final nameController = TextEditingController();
  final topicController = TextEditingController();
  final sumquestionController = TextEditingController();
  final timeController = TextEditingController();
  final scoreController = TextEditingController();

  @override
  void initState() {
    super.initState();

    nameController.addListener(() => setState(() {}));
    topicController.addListener(() => setState(() {}));
    sumquestionController.addListener(() => setState(() {}));
    timeController.addListener(() => setState(() {}));
    scoreController.addListener(() => setState(() {}));
  }

  @override
  void didChangeDependencies() {
    if (widget.id != null) {
      Quiz prev =
          Provider.of<GState>(context, listen: false).singleQuiz(widget.id!);
      nameController.text = prev.name;
      topicController.text = prev.topic;
      sumquestionController.text = prev.numberOfQuestion.toString();
      timeController.text = prev.time.toString();
      scoreController.text = prev.requiredScoreToPass.toString();
    }
    super.didChangeDependencies();
  }

  void _addQuiz() {
    if (!_form.currentState!.validate()) {
      return;
    }

    _form.currentState!.save();
    print(_name);
    print(_topic);
    print(_number_of_question);
    print(_time);
    print(_required_score);

    if (widget.id != null) {
      Provider.of<GState>(context, listen: false).updateQuiz(widget.id!, _name,
          _topic, _number_of_question, _time, _required_score);
    } else {
      Provider.of<GState>(context, listen: false).addNewQuiz(
          _name, _topic, _number_of_question, _time, _required_score);
    }
    Navigator.of(context).pop();
  }

  Widget _buildNameField() {
    return TextFormField(
      controller: nameController,
      decoration: InputDecoration(
        labelText: 'Name',
        suffixIcon: nameController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => nameController.clear(),
              ),
      ),
      validator: (String? val) {
        if (val!.isEmpty) {
          return 'Name is required';
        }
        return null;
      },
      onSaved: (String? val) {
        _name = val!;
      },
    );
  }

  Widget _buildTopicField() {
    return TextFormField(
      controller: topicController,
      decoration: InputDecoration(
        labelText: 'Topic',
        suffixIcon: topicController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => topicController.clear(),
              ),
      ),
      validator: (String? val) {
        if (val!.isEmpty) {
          return 'Topic is required';
        }
        return null;
      },
      onSaved: (String? val) {
        _topic = val!;
      },
    );
  }

  Widget _buildNumberField() {
    return TextFormField(
      controller: sumquestionController,
      decoration: InputDecoration(
        labelText: 'Number of question',
        suffixIcon: sumquestionController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => sumquestionController.clear(),
              ),
      ),
      keyboardType: TextInputType.number,
      validator: (String? val) {
        var num = int.tryParse(val!);

        if (num == null || num <= 0) {
          return 'Number of question must be greater than 0';
        }
        return null;
      },
      onSaved: (String? val) {
        _number_of_question = int.tryParse(val!)!;
      },
    );
  }

  Widget _buildTimeField() {
    return TextFormField(
      controller: timeController,
      decoration: InputDecoration(
        labelText: 'Time Limit in Minutes',
        suffixIcon: timeController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => timeController.clear(),
              ),
      ),
      keyboardType: TextInputType.number,
      validator: (String? val) {
        var num = int.tryParse(val!);

        if (num == null || num <= 0) {
          return 'Time limit must be greater than 0';
        }
        return null;
      },
      onSaved: (String? val) {
        _time = int.tryParse(val!)!;
      },
    );
  }

  Widget _buildScoreField() {
    return TextFormField(
      controller: scoreController,
      decoration: InputDecoration(
        labelText: 'Required Score to Pass',
        suffixIcon: scoreController.text.isEmpty
            ? Container(width: 0)
            : IconButton(
                icon: Icon(Icons.close),
                onPressed: () => scoreController.clear(),
              ),
      ),
      keyboardType: TextInputType.number,
      validator: (String? val) {
        var num = int.tryParse(val!);

        if (num == null || num <= 0) {
          return 'Score must be greater than 0';
        }
        return null;
      },
      onSaved: (String? val) {
        _required_score = int.tryParse(val!)!;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.id != null ? 'Edit Quiz' : 'Add Quiz'),
      ),
      body: Container(
        padding: EdgeInsets.all(24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ListView(
              shrinkWrap: true,
              children: [
                Form(
                  key: _form,
                  child: Column(
                    children: <Widget>[
                      _buildNameField(),
                      SizedBox(
                        height: 6,
                      ),
                      _buildTopicField(),
                      SizedBox(
                        height: 6,
                      ),
                      _buildNumberField(),
                      SizedBox(
                        height: 6,
                      ),
                      _buildTimeField(),
                      SizedBox(
                        height: 6,
                      ),
                      _buildScoreField(),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 24),
                color: Colors.purple,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Text(
                  widget.id != null ? 'Edit Quiz' : 'Add Quiz',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
                onPressed: () {
                  _addQuiz();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
