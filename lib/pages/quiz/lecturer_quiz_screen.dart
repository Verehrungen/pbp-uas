import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/pages/quiz/add_edit_screen.dart';
import 'package:tkpbp/pages/quiz/quiz_detail_screen.dart';
import 'package:tkpbp/models/global_state.dart';

class LecturerQuizScreen extends StatefulWidget {
  @override
  _LecturerQuizScreenState createState() => _LecturerQuizScreenState();
}

class _LecturerQuizScreenState extends State<LecturerQuizScreen> {
  @override
  void didChangeDependencies() {
    Provider.of<GState>(context, listen: false).getQuizzesData();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final quizzesData = Provider.of<GState>(context).quizzes;
    // print(quizzesData);
    return Scaffold(
        appBar: AppBar(
          title: Text('Quiz List'),
        ),
        body: quizzesData.isEmpty
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'No Quiz to Display',
                    ),
                  ],
                ),
              )
            : Padding(
                padding: EdgeInsets.all(12),
                child: ListView.builder(
                  itemCount: quizzesData.length,
                  itemBuilder: (context, index) {
                    var item = quizzesData[index];
                    return Card(
                      clipBehavior: Clip.antiAlias,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: InkWell(
                        hoverColor: Colors.black12,
                        child: Container(
                          padding: EdgeInsets.all(12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "${item.name} - ${item.topic}",
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(height: 4),
                                  Text(
                                    "Time Limit: ${item.time} minutes \nNumber of Questions: ${item.numberOfQuestion} \nRequired Score to Pass: ${item.requiredScoreToPass}",
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.grey,
                                    ),
                                  ),
                                ],
                              ),
                              Spacer(),
                              IconButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          AddEditScreen(item.id)));
                                },
                                icon: Icon(Icons.edit),
                                color: Colors.green,
                              ),
                              IconButton(
                                onPressed: () {
                                  Provider.of<GState>(context, listen: false)
                                      .deleteQuiz(item.id);
                                },
                                icon: Icon(Icons.delete),
                                color: Colors.red,
                              )
                            ],
                          ),
                        ),
                        onTap: () {
                          print(item.id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) =>
                                  DetailQuizScreen(id: item.id)));
                        },
                      ),
                    );
                  },
                ),
              ),
        floatingActionButton: FloatingActionButton.extended(
          icon: Icon(Icons.add),
          onPressed: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => AddEditScreen()));
          },
          label: Text('Add Quiz'),
          backgroundColor: Colors.purple,
          foregroundColor: Colors.white,
        ));
  }
}
