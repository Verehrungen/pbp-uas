import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:tkpbp/models/contentfile.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

/// This is the stateful widget that the main application instantiates.
class ContentWidget extends StatefulWidget {
  const ContentWidget({Key? key}) : super(key: key);

  @override
  State<ContentWidget> createState() => _ContentWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _ContentWidgetState extends State<ContentWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static TextEditingController controller1 = TextEditingController();
  static TextEditingController controller2 = TextEditingController();
  List<String> array1 = ["Google", "Facebook", "Twitter"];
  List<String> array2 = [
    "https://www.google.com",
    "https://www.facebook.com",
    "https://www.twitter.com"
  ];
  Future<void> postSectionData(BuildContext context) async {
    var response = await http.post(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/add-content/1/"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        },
        body:
            jsonEncode({'title': controller1.text, 'file': controller2.text}));
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        // sectionsLst = sectionFromMap(response.body);
        // widget.course.sections = sectionsLst;
      });
    } else {
      print("Error occured");
    }
  }

  // @override
  // void dispose() {
  //   // Clean up the controller when the widget is disposed.
  //   controller1.dispose();
  //   controller2.dispose();
  //   super.dispose();
  // }

  clearTextInput() {
    controller1.clear();
    controller2.clear();
  }

  _getRequests() async {}

  getArray1() {
    return array1;
  }

  getArray2() {
    return array2;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Content"),
          centerTitle: true,
          backgroundColor: Colors.purple,
        ),
        body: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text('Add Content',
                    style: TextStyle(
                      fontSize: 34,
                      height: 2.0,
                    )),
                Text('This field is required.',
                    style: TextStyle(
                      fontSize: 14,
                      height: 2.0,
                    )),
                Text('Title:',
                    style: TextStyle(
                      fontSize: 18,
                      height: 2.0,
                    )),
                TextFormField(
                  controller: controller1,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Fill out this field';
                    }
                    return null;
                  },
                ),
                Text('This field is required.',
                    style: TextStyle(
                      fontSize: 14,
                      height: 2.0,
                    )),
                Text('File Link:',
                    style: TextStyle(
                      fontSize: 18,
                      height: 2.0,
                    )),
                TextFormField(
                  controller: controller2,
                  validator: (String? value) {
                    if (value == null || value.isEmpty) {
                      return 'Fill out this field';
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        try {
                          await postSectionData(context);
                          showDialog<String>(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                              title: Row(children: <Widget>[
                                new RichText(
                                  text: new TextSpan(
                                    children: [
                                      new TextSpan(
                                        text: controller1.text,
                                        style:
                                            new TextStyle(color: Colors.blue),
                                        recognizer: new TapGestureRecognizer()
                                          ..onTap = () {
                                            launch(controller2.text);
                                          },
                                      ),
                                    ],
                                  ),
                                ),
                              ]),
                            ),
                          );
                          Future.delayed(
                              const Duration(seconds: 2),
                              () => Navigator.of(context)
                                  .push(
                                    new MaterialPageRoute(
                                        builder: (_) =>
                                            new DestinationScreen()),
                                  )
                                  .then((val) => val ? _getRequests() : null));

                          // clearTextInput();
                        } catch (e) {}
                      }
                    },
                    child: const Text('SUBMIT'),
                    style: ElevatedButton.styleFrom(primary: Colors.green),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class DestinationScreen extends StatefulWidget {
  DestinationScreen({Key? key}) : super(key: key);

  @override
  _DestinationScreenState createState() => _DestinationScreenState();
}

class _DestinationScreenState extends State<DestinationScreen> {
  List<ContentFile>? lstContent;
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      _getData();
    });
    super.initState();
  }

  Future<void> _getData() async {
    await getJsonData(context);
  }

  Future<void> getJsonData(BuildContext context) async {
    var response = await http.get(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/all-contents"),
        headers: {
          "Accept": "aplication/json",
        });
    // print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        lstContent = contentFileFromJson(response.body);
      });
    } else {
      print("Error occured");
    }
  }

  static const String _title = 'Learning App';
  final statefulwidg = _ContentWidgetState();

  bool _isVisible = true;

  void showToast() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return lstContent == null
        ? Scaffold(
            appBar: AppBar(
              title: const Text(_title),
              centerTitle: true,
            ),
            body: Center(child: CircularProgressIndicator()))
        : Scaffold(
            appBar: AppBar(
              title: const Text(_title),
              centerTitle: true,
              backgroundColor: Colors.purple,
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ContentWidget(),
                    ));
              },
              child: Icon(Icons.add),
              backgroundColor: Colors.green,
            ),
            floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  for (var i = 0; i < 3; i++)
                    Visibility(
                      visible: _isVisible,
                      child: Card(
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.grey.withOpacity(0.2),
                              width: 1,
                            ),
                          ),
                          child: Container(
                              width: 350,
                              height: 50,
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15, 8, 15, 8),
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new RichText(
                                        textAlign: TextAlign.center,
                                        text: new TextSpan(
                                          children: [
                                            new TextSpan(
                                              text: statefulwidg.getArray1()[i],
                                              style: new TextStyle(
                                                  color: Colors.blue),
                                              recognizer:
                                                  new TapGestureRecognizer()
                                                    ..onTap = () {
                                                      launch(statefulwidg
                                                          .getArray2()[i]);
                                                    },
                                            ),
                                          ],
                                        ),
                                      ),
                                      ElevatedButton(
                                        onPressed: showToast,
                                        child: const Text('DELETE'),
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.red),
                                      )
                                    ]),
                              ))),
                    ),
                  for (var file in lstContent!)
                    Visibility(
                      visible: _isVisible,
                      child: Card(
                          shape: RoundedRectangleBorder(
                            side: BorderSide(
                              color: Colors.grey.withOpacity(0.2),
                              width: 1,
                            ),
                          ),
                          child: Container(
                              width: 350,
                              height: 50,
                              child: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(15, 8, 15, 8),
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new RichText(
                                        textAlign: TextAlign.center,
                                        text: new TextSpan(
                                          children: [
                                            new TextSpan(
                                              text: file.fields!.title!,
                                              style: new TextStyle(
                                                  color: Colors.blue),
                                              recognizer:
                                                  new TapGestureRecognizer()
                                                    ..onTap = () {
                                                      launch(
                                                          file.fields!.file!);
                                                    },
                                            ),
                                          ],
                                        ),
                                      ),
                                      ElevatedButton(
                                        onPressed: showToast,
                                        child: const Text('DELETE'),
                                        style: ElevatedButton.styleFrom(
                                            primary: Colors.red),
                                      )
                                    ]),
                              ))),
                    ),
                ],
              ),
            ),
          );
  }
}
