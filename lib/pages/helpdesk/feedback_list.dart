import 'package:flutter/material.dart';
import 'package:tkpbp/models/feedback.dart';
import 'home_helpdesk.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class FeedbackList extends StatefulWidget {
  @override
  State<FeedbackList> createState() => _FeedbackListState();
}

class _FeedbackListState extends State<FeedbackList> {
  List<MyFeedback>? lstFeedback;
  bool isloading = true;
  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await getSectionData(context);
      setState(() {
        isloading = false;
      });
    });

    super.initState();
  }

  Future<void> getSectionData(BuildContext context) async {
    var response = await http.get(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/all-feedbacks/"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        lstFeedback = myFeedbackFromJson(response.body);
      });
    } else {
      print("Error occured");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Feedback List'),
        backgroundColor: Colors.purple[500],
      ),
      body: isloading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.all(25.0),
              child: ListView(
                children: <Widget>[
                  Text(
                    'Feedback List',
                    style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      top: 20.0,
                    ),
                  ),
                  for (var feed in lstFeedback!)
                    Card(
                      elevation: 10.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      color: Color.fromRGBO(141, 129, 204, 1),
                      child: Container(
                        height: 140,
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '${feed.fields.froma}',
                              style: TextStyle(
                                  fontSize: 24.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Poppins'),
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              '${feed.fields.message}',
                              style: TextStyle(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontFamily: 'Poppins'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 20.0),
                      RaisedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => HomeHelpdesk()));
                        },
                        elevation: 10.0,
                        color: Color.fromRGBO(141, 129, 204, 1),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0))),
                        child: Text(
                          'Back To Hepldesk',
                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}
