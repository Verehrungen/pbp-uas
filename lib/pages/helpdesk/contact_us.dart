import 'package:flutter/material.dart';
import 'home_helpdesk.dart';
import 'dart:convert';

class ContactUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HELPDESK'),
        backgroundColor: Colors.purple[500],
      ),
      body: Container(
        padding: EdgeInsets.all(25.0),
        child: ListView(
          children: <Widget>[
            Text(
              'Contact Us',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 20.0,
              ),
            ),

            Card(
              elevation: 10.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              color: Color.fromRGBO(141, 129, 204, 1),
              child: Container(
                height: 200,
                padding: EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: 30.0),
                    Text(
                      'Email: admin@learningapp.com',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontFamily: 'Poppins'),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Number: 08123456789',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontFamily: 'Poppins'),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Address: Jalan Kemerdekaan No. 45',
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontFamily: 'Poppins'),
                    ),
                  ],
                ),
              ),
            ), //card

            Padding(
              padding: EdgeInsets.only(
                top: 20.0,
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 20.0),
                RaisedButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomeHelpdesk()));
                  },
                  elevation: 10.0,
                  color: Color.fromRGBO(141, 129, 204, 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Text(
                    'Back To Hepldesk',
                    style: TextStyle(color: Colors.white, fontSize: 15.0),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
