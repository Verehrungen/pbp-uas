import 'package:flutter/material.dart';
import 'home_helpdesk.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class FeedbackForm extends StatelessWidget {
  const FeedbackForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Send Your Feedback';

    return Scaffold(
      body: MyCustomForm(),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  String nama = "";
  String message = "";

  Future<void> postSectionData(BuildContext context) async {
    var response = await http.post(
        Uri.parse("https://aplikasi-pbp.herokuapp.com/api/add-feedback/"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        },
        body: jsonEncode({'from': nama, 'msg': message}));
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
      });
    } else {
      print("Error occured");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HELPDESK"),
        backgroundColor: Colors.purple[500],
      ),
      body: Padding(
        padding: EdgeInsets.only(
          left: 25.0,
          right: 25.0,
          bottom: 20.0,
        ),
        child: Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Send Your Feedback',
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                ),
                TextFormField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(
                          color: Color.fromRGBO(141, 129, 204, 1),
                        ),
                      ),
                      labelText: "Name"),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add your name';
                    }
                    nama = value;
                    return null;
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                ),
                TextFormField(
                  maxLines: 7,
                  decoration: InputDecoration(
                    labelText: "Feedback",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Color.fromRGBO(141, 129, 204, 1),
                      ),
                    ),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please add your feedback';
                    }
                    message = value;
                    return null;
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        color: Color.fromRGBO(141, 129, 204, 1),
                        onPressed: () async {
                          if (_formKey.currentState!.validate()) {
                            await postSectionData(context);
                            print("From: " + nama);
                            print("Message:\n" + message);

                            showDialog(
                                context: context,
                                builder: (context) {
                                  return AlertDialog(
                                    content: Text(
                                        "Succesfully send your feedback\n\nFrom: " +
                                            nama +
                                            "\nMessage:\n" +
                                            message),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      HomeHelpdesk()));
                                        },
                                        child: Text('OK'),
                                      ),
                                    ],
                                  );
                                });
                          }
                        },
                        child: Text(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: 40.0,
                        ),
                      ),
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        color: Color.fromRGBO(141, 129, 204, 1),
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => HomeHelpdesk()));
                        },
                        child: Text("Cancel",
                            style: TextStyle(color: Colors.white)),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
