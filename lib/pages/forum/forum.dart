import 'package:flutter/material.dart';
import 'package:tkpbp/models/forumpost.dart';
import 'package:tkpbp/pages/forum/thread.dart';
import 'package:tkpbp/widgets/main_drawer.dart';
import 'package:tkpbp/pages/forum/forum.dart';
import 'package:tkpbp/models/user.dart';
import 'package:tkpbp/models/course/course.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class Forum extends StatefulWidget {
  Forum({Key? key, required this.course, required this.loggedInUser})
      : super(key: key);
  List<User>? loggedInUser;
  Courses course;

  @override
  State<Forum> createState() => _ForumState();
}

class _ForumState extends State<Forum> {
  bool isloading = true;
  var count = 0;
  var title = TextEditingController();
  var content = TextEditingController();
  var listThread = <ForumPost>[];
  var allUsers = <User>[];
  var authors = <User>[];
  @override // Figure this shit out.
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      getAllUser();
      await _getData();
      isloading = false;
    });
    super.initState();
  }

  void getAuthor() {
    for (var thread in listThread) {
      for (var users in allUsers) {
        if (thread.fields!.author == users.pk) {
          setState(() {
            authors.add(users);
          });
        }
      }
    }
  }

  Future<void> _getData() async {
    await getJsonData(context);
    getAuthor();
  }

  Future<void> getJsonData(BuildContext context) async {
    var response = await http.get(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/forum/main/${widget.course.pk}"),
        headers: {
          "Accept": "application/json", // Figure this out later
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        listThread = postFromJson(response.body);
        getAuthor(); // figure this out
      });
    } else {
      print("Error occured");
    }
  }

  void getAllUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("myData")) {
      var myData =
          json.decode(localData.getString("myData")!) as Map<String, dynamic>;
      allUsers = userFromMap(myData['all_user']!.toString());
      print(allUsers);
    }
  }

  Future<void> postSectionData(BuildContext context) async {
    setState(() {
      isloading = true;
    });
    var response = await http.post(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/forum/createpost/${widget.course.pk}/${widget.loggedInUser![0].pk}"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        },
        body: jsonEncode({'title': title.text, 'content': content.text}));
    print(response.body);
    if (response.statusCode == 200) {
      await getJsonData(context);
    } else {
      print("Error occured");
    }

    setState(() {
      isloading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Forum'),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back))
        ],
      ),
      drawer: MainDrawer(
        loggedInUser: widget.loggedInUser,
      ),
      body: isloading
          ? Center(child: CircularProgressIndicator())
          : Padding(
              padding: EdgeInsets.all(8),
              child: ListView.builder(
                  itemCount: listThread.length, //jumlah diskusi
                  itemBuilder: (context, index) {
                    return Card(
                        elevation: 10,
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ThreadContents(
                                        thread: listThread[index],
                                        loggedInUser: widget.loggedInUser)));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(25.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${listThread[index].fields!.title ?? '0'}', // Ini buat Title
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 22),
                                ),
                                SizedBox(
                                  height: 30, // White space
                                ),
                                Text(
                                  'posted by ${authors[index].fields!.firstName ?? listThread[index].fields!.author}' +
                                      ' ' +
                                      '${authors[index].fields!.lastName}'
                                          ' on ' +
                                      '${listThread[index].fields!.timestamp}', // Author and dates
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14),
                                ),
                                SizedBox(
                                  height: 14, // White space
                                ),
                                Text(
                                  '${listThread[index].fields!.content}', // Content
                                  style: TextStyle(fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                        )); // PROBLEM CHILD
                  }),
            ),
      floatingActionButton: FloatingActionButton(
        //onPressed, pergi ke popup add
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  scrollable: true,
                  title: Text('Add to Discussion'),
                  content: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Form(
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            controller: title,
                            decoration: InputDecoration(
                                labelText: 'Title',
                                icon: Icon(Icons.account_box)),
                          ),
                          TextFormField(
                            controller: content,
                            decoration: InputDecoration(
                                labelText: 'Content',
                                icon: Icon(Icons.account_box)),
                            maxLines: 5,
                            minLines: 3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    ElevatedButton(
                        child: Text("Submit"),
                        onPressed: () async {
                          print("lol");
                          // kodeeee
                          await postSectionData(context);
                        })
                  ],
                );
              });
        }, //popup form
        tooltip: 'Add to Discussion',
        child: const Icon(Icons.add),
      ),
    );
  }
}
