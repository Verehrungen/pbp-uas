import 'package:flutter/material.dart';
import 'package:tkpbp/models/forumpost.dart';
import 'package:tkpbp/models/reply.dart';
import 'package:tkpbp/models/user.dart';
import 'package:tkpbp/widgets/main_drawer.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ThreadContents extends StatefulWidget {
  ThreadContents({Key? key, required this.thread, required this.loggedInUser})
      : super(key: key);
  List<User>? loggedInUser;
  ForumPost thread;
  @override
  _ThreadContentsState createState() => _ThreadContentsState();
}

class _ThreadContentsState extends State<ThreadContents> {
  var listReply = <Reply>[];
  var allUsers = <User>[];
  var authors = <User>[];
  @override // Figure this shit out.
  void initState() {
    getAllUser();
    _getData();
    super.initState();
  }

  void getAuthor() {
    for (var thread in listReply) {
      for (var users in allUsers) {
        if (thread.fields!.author == users.pk) {
          setState(() {
            authors.add(users);
          });
        }
      }
    }
  }

  Future<void> _getData() async {
    await getJsonData(context);
    getAuthor();
  }

  Future<void> getJsonData(BuildContext context) async {
    var response = await http.get(
        Uri.parse(
            "https://aplikasi-pbp.herokuapp.com/api/forum/${widget.thread.pk}"),
        headers: {
          "Accept": "application/json", // Figure this out later
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        listReply = replyFromJson(response.body); // figure this out
      });
    } else {
      print("Error occured");
    }
  }

  void getAllUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("myData")) {
      var myData =
          json.decode(localData.getString("myData")!) as Map<String, dynamic>;
      allUsers = userFromMap(myData['all_user']!.toString());
      print(allUsers);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Forum'),
        leading: Builder(
          builder: (context) => IconButton(
            icon: Icon(
              Icons.drag_indicator,
            ),
            color: Colors.white,
            onPressed: () => Scaffold.of(context).openDrawer(),
          ),
        ),
        backgroundColor: Colors.purple,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back))
        ],
      ),
      drawer: MainDrawer(
        loggedInUser: widget.loggedInUser,
      ),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: ListView.builder(
            itemCount: listReply.length, //jumlah diskusi
            itemBuilder: (context, index) {
              return Card(
                elevation: 10,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'posted by ${authors[index].fields!.firstName ?? listReply[index].fields!.author}' +
                            ' ' +
                            '${authors[index].fields!.lastName}'
                                ' on ' +
                            '${listReply[index].fields!.timestamp}', // Author and dates
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 14),
                      ),
                      SizedBox(
                        height: 14, // White space
                      ),
                      Text(
                        '${listReply[index].fields!.content}', // Content
                        style: TextStyle(fontSize: 14),
                      ),
                    ],
                  ),
                ),
              ); // PROBLEM CHILD
            }),
      ),
      floatingActionButton: FloatingActionButton(
        //onPressed, pergi ke popup add
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  scrollable: true,
                  title: Text('Add to Discussion'),
                  content: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Form(
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            decoration: InputDecoration(
                                labelText: 'Content',
                                icon: Icon(Icons.account_box)),
                            maxLines: 5,
                            minLines: 3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  actions: [
                    ElevatedButton(
                        child: Text("Submit"),
                        onPressed: () {
                          print("lol");
                          // your code
                        })
                  ],
                );
              });
        }, //popup form
        tooltip: 'Add to Discussion',
        child: const Icon(Icons.add),
      ),
    );
  }
}
