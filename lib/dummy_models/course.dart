//**
//
//
//
// DUMMY
//
//
//
//*/

import 'package:tkpbp/dummy_models/section.dart';

class Course {
  String? author;
  String? title;
  String? description;
  // final DateTime timeCreated;
  List<Section>? sections;

  // Course.sections(this.sections);
  Course(this.author, this.title, this.description, this.sections);
}

var listCrs = [
  Course(
      "Dosen 1",
      "Pemrograman berbasis platform",
      "Sed porttitor lectus nibh. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.",
      [
        Section('Week01', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week02', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week03', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week04', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
      ]),
  Course(
      "Dosen 2",
      "Struktur data algoritma",
      "Pellentesque in ipsum id orci porta dapibus. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Donec rutrum congue leo eget malesuada. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat.",
      [
        Section('Week01', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week02', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week03', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week04', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
      ]),
  Course(
      "Pak Guru",
      "Aljabar Linier",
      "Nulla quis lorem ut libero malesuada feugiat. Curabitur non nulla sit amet nisl tempus convallis quis ac lectus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Proin eget tortor risus. Donec rutrum congue leo eget malesua",
      [
        Section('Week01', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week02', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week03', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week04', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
      ]),
  Course(
      "Bu Guru",
      "MPKT-B",
      "Cras ultricies ligula sed magna dictum porta. Donec sollicitudin molestie malesuada. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Pellentesque in ipsum id orci porta dapibus.",
      [
        Section('Week01', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week02', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week03', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week04', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
      ]),
  Course(
      "Guru 3",
      "Rekayasa Perangkat Lunak",
      "Proin eget tortor risus. Quisque velit nisi, pretium ut lacinia in, elementum id enim. Sed porttitor lectus nibh. Vivamus suscipit tortor eget felis porttitor volutpat.",
      [
        Section('Week01', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week02', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week03', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week04', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
      ]),
  Course(
      "Guruku",
      "Kombistek",
      "Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed, convallis at tellus. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Donec sollicitudin molestie malesuada.",
      [
        Section('Week01', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week02', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week03', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
        Section('Week04', [
          'Sed porttitor lectus nibh',
          'Nulla quis lorem ut libero malesuada feugiat. Donec sollicitudin molestie malesuada. Curabitur non nulla',
          'ut libero malesuada',
          'Curabitur non nulla sit a'
        ]),
      ])
];
