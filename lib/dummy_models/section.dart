//**
//
//
//
// DUMMY
//
//
//
//*/

class Section {
  String? title;
  List<String> contents = <String>[];
  bool isExpanded = false;

  Section(this.title, this.contents);
}
