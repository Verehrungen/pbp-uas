// To parse this JSON data, do
//
//     final myFeedback = myFeedbackFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

List<MyFeedback> myFeedbackFromJson(String str) =>
    List<MyFeedback>.from(json.decode(str).map((x) => MyFeedback.fromJson(x)));

String myFeedbackToJson(List<MyFeedback> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class MyFeedback {
  MyFeedback({
    required this.model,
    required this.pk,
    required this.fields,
  });

  String model;
  int pk;
  Fields fields;

  factory MyFeedback.fromJson(Map<String, dynamic> json) => MyFeedback(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields.toJson(),
      };
}

class Fields {
  Fields({
    required this.froma,
    required this.message,
  });

  String froma;
  String message;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        froma: json["froma"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "froma": froma,
        "message": message,
      };
}
