//https://app.quicktype.io/
//
//To parse this JSON data, do
//
//     final courses = coursesFromJson(jsonString);

import 'dart:convert';

import 'package:tkpbp/models/course/section.dart';

List<Courses> coursesFromJson(String str) =>
    List<Courses>.from(json.decode(str).map((x) => Courses.fromJson(x)));

String coursesToJson(List<Courses> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Courses {
  Courses({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  List<Section> sections = <Section>[];

  factory Courses.fromJson(Map<String, dynamic> json) => Courses(
        model: json["model"]!,
        pk: json["pk"]!,
        fields: json["fields"] == null ? null : Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model!,
        "pk": pk!,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.author,
    this.title,
    this.subtitle,
    this.description,
  });

  int? author;
  String? title;
  String? subtitle;
  String? description;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        author: json["author"]!,
        title: json["title"]!,
        subtitle: json["subtitle"]!,
        description: json["description"]!,
      );

  Map<String, dynamic> toJson() => {
        "author": author!,
        "title": title!,
        "subtitle": subtitle!,
        "description": description!,
      };
}
