// To parse this JSON data, do
//
//     final section = sectionFromMap(jsonString);

import 'dart:convert';

import 'package:tkpbp/models/course/course_post.dart';

List<Section> sectionFromMap(String str) =>
    List<Section>.from(json.decode(str).map((x) => Section.fromMap(x)));

String sectionToMap(List<Section> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Section {
  Section({
    this.model,
    this.pk,
    this.fields,
  });

  Model? model;
  int? pk;
  SectionFields? fields;

  List<CoursePost> posts = <CoursePost>[];

  factory Section.fromMap(Map<String, dynamic> json) => Section(
        model: modelValues.map![json["model"]],
        pk: json["pk"],
        fields: SectionFields.fromMap(json["fields"]),
      );

  Map<String, dynamic> toMap() => {
        "model": modelValues.reverse[model],
        "pk": pk,
        "fields": fields!.toMap(),
      };
}

class SectionFields {
  SectionFields({
    this.course,
    this.title,
  });

  int? course;
  String? title;

  factory SectionFields.fromMap(Map<String, dynamic> json) => SectionFields(
        course: json["course"],
        title: json["title"],
      );

  Map<String, dynamic> toMap() => {
        "course": course,
        "title": title,
      };
}

enum Model { MAIN_SECTION }

final modelValues = EnumValues({"main.section": Model.MAIN_SECTION});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map!.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap!;
  }
}
