// To parse this JSON data, do
//
//     final coursePost = coursePostFromMap(jsonString);

import 'dart:convert';

List<CoursePost> coursePostFromMap(String str) =>
    List<CoursePost>.from(json.decode(str).map((x) => CoursePost.fromMap(x)));

String coursePostToMap(List<CoursePost> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class CoursePost {
  CoursePost({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  CoursePostFields? fields;
  bool isExpanded = false;

  factory CoursePost.fromMap(Map<String, dynamic> json) => CoursePost(
        model: json["model"],
        pk: json["pk"],
        fields: CoursePostFields.fromMap(json["fields"]),
      );

  Map<String, dynamic> toMap() => {
        "model": model,
        "pk": pk,
        "fields": fields!.toMap(),
      };
}

class CoursePostFields {
  CoursePostFields({
    this.course,
    this.section,
    this.title,
    this.description,
  });

  int? course;
  int? section;
  String? title;
  String? description;

  factory CoursePostFields.fromMap(Map<String, dynamic> json) =>
      CoursePostFields(
        course: json["course"],
        section: json["section"],
        title: json["title"],
        description: json["description"],
      );

  Map<String, dynamic> toMap() => {
        "course": course,
        "section": section,
        "title": title,
        "description": description,
      };
}
