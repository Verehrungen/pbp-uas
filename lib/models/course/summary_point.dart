// To parse this JSON data, do
//
//     final summaryPoint = summaryPointFromJson(jsonString);

import 'dart:convert';

List<SummaryPoint> summaryPointFromJson(String str) => List<SummaryPoint>.from(
    json.decode(str).map((x) => SummaryPoint.fromJson(x)));

String summaryPointToJson(List<SummaryPoint> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SummaryPoint {
  SummaryPoint({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  factory SummaryPoint.fromJson(Map<String, dynamic> json) => SummaryPoint(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.course,
    this.title,
  });

  int? course;
  String? title;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        course: json["course"],
        title: json["title"],
      );

  Map<String, dynamic> toJson() => {
        "course": course,
        "title": title,
      };
}
