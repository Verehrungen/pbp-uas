import 'dart:convert';

List<ForumPost> postFromJson(String str) =>
    List<ForumPost>.from(json.decode(str).map((x) => ForumPost.fromJson(x)));

String postToJson(List<ForumPost> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ForumPost {
  ForumPost({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  factory ForumPost.fromJson(Map<String, dynamic> json) => ForumPost(
        model: json["model"]!,
        pk: json["pk"]!,
        fields: json["fields"] == null ? null : Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model!,
        "pk": pk!,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.course,
    this.author,
    this.title,
    this.content,
    this.timestamp,
  });

  int? course;
  int? author;
  String? title;
  String? content;
  DateTime? timestamp;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        course: json["course"]!,
        author: json["author"]!,
        title: json["title"]!,
        content: json["content"]!,
        timestamp: DateTime.parse(json["timestamp"]!),
      );

  Map<String, dynamic> toJson() => {
        "course": course,
        "author": author!,
        "title": title!,
        "content": content!,
        "timestamp": timestamp?.toIso8601String(),
      };
}
