class Content {
  final int course;
  final String title;
  final String file;

  Content({required this.course, required this.title, required this.file});

  factory Content.fromJson(Map<String, dynamic> json) {
    return Content(
        course: json['course'], title: json['title'], file: json['file']);
  }
}
