import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class Quiz {
  int id;
  String name;
  String topic;
  int numberOfQuestion;
  int time;
  int requiredScoreToPass;

  Quiz(
      {required this.id,
      required this.name,
      required this.topic,
      required this.numberOfQuestion,
      required this.time,
      required this.requiredScoreToPass});
}

class Question {
  int id;
  String text;
  int quizId;

  Question({
    required this.id,
    required this.text,
    required this.quizId,
  });
}

class CompleteQuestion {
  int id;
  String text;
  int quizId;
  List<Answer> options;
  int answerIndex;

  CompleteQuestion({
    required this.id,
    required this.text,
    required this.quizId,
    required this.options,
    required this.answerIndex,
  });
}

class Answer {
  int id;
  String text;
  bool correct;
  int questionId;

  Answer({
    required this.id,
    required this.text,
    required this.correct,
    required this.questionId,
  });
}

class GState with ChangeNotifier {
  String urlsQuiz = 'http://aplikasi-pbp.herokuapp.com/quiz/quizapi/ujian/';
  String urlsQuestion =
      'http://aplikasi-pbp.herokuapp.com/quiz/quizapi/question/';
  String urlsAnswer = 'http://aplikasi-pbp.herokuapp.com/quiz/quizapi/answer/';
  List<Quiz> _quizzes = [];
  List<Question> _questions = [];
  List<Answer> _answers = [];
  List<Question> _questionsOfQuiz = [];
  List<Answer> _answersOfQuestions = [];

  Future<void> getQuizzesData() async {
    http.Response response = await http.get(Uri.parse(urlsQuiz));
    if (response.statusCode == 200) {
      var data = json.decode(response.body) as List;
      List<Quiz> quizzes = [];
      data.forEach((element) {
        Quiz q = Quiz(
            id: element["id"],
            name: element["name"],
            topic: element["topic"],
            numberOfQuestion: element["number_of_questions"],
            time: element["time"],
            requiredScoreToPass: element["required_score_to_pass"]);
        quizzes.add(q);
      });
      _quizzes = quizzes;
      notifyListeners();
    } else {
      print("No Data Found");
    }
  }

  Future<void> getQuestionData() async {
    http.Response response = await http.get(Uri.parse(urlsQuestion));
    if (response.statusCode == 200) {
      var data = json.decode(response.body) as List;
      List<Question> questions = [];
      data.forEach((element) {
        Question q = Question(
            id: element["id"], text: element["text"], quizId: element["quiz"]);
        questions.add(q);
      });
      _questions = questions;
      notifyListeners();
    } else {
      print("No Data Found");
    }
  }

  Future<void> getAnswerData() async {
    http.Response response = await http.get(Uri.parse(urlsAnswer));
    if (response.statusCode == 200) {
      var data = json.decode(response.body) as List;
      List<Answer> answers = [];
      data.forEach((element) {
        Answer q = Answer(
            id: element["id"],
            text: element["text"],
            correct: element["correct"],
            questionId: element["question"]);
        answers.add(q);
      });
      _answers = answers;
      notifyListeners();
    } else {
      print("No Data Found");
    }
  }

  Future<void> getQuestionOfQuiz(int id) async {
    String url = 'http://aplikasi-pbp.herokuapp.com/quiz/quizapi/question/$id/';
    List<Question> tmp = [];

    http.Response response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      _questionsOfQuiz = [];
      var data = json.decode(response.body) as List;
      data.forEach((element) {
        Question q = Question(
            id: element["id"], text: element["text"], quizId: element["quiz"]);
        tmp.add(q);
      });
      _questionsOfQuiz = tmp;
      notifyListeners();
    } else {
      print("No Data Found");
    }
  }

  Future<void> getAnswerOfQuestion(int id) async {
    String url = 'http://aplikasi-pbp.herokuapp.com/quiz/quizapi/answer/$id/';
    List<Answer> tmp = [];

    http.Response response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      _answersOfQuestions = [];
      var data = json.decode(response.body) as List;
      data.forEach((element) {
        Answer q = Answer(
            id: element["id"],
            text: element["text"],
            correct: element["correct"],
            questionId: element["question"]);
        tmp.add(q);
      });
      _answersOfQuestions = tmp;
      notifyListeners();
    } else {
      print("No Data Found");
    }
  }

  List<Quiz> get quizzes {
    return [..._quizzes];
  }

  List<Question> get questions {
    return [..._questions];
  }

  List<Question> get questionOfQuiz {
    return [..._questionsOfQuiz];
  }

  List<Answer> get answerOfQuestion {
    return [..._answersOfQuestions];
  }

  Future<void> addNewQuiz(String name, String topic, int numberOfQuestion,
      int time, int requiredScoreToPass) async {
    try {
      http.Response response = await http.post(
        Uri.parse(urlsQuiz),
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "name": name,
          "topic": topic,
          "number_of_questions": numberOfQuestion,
          "time": time,
          "required_score_to_pass": requiredScoreToPass
        }),
      );
      print(response.body);
    } catch (e) {
      print(e);
    }
  }

  Future<void> addNewQuestion(String text, int quiz) async {
    try {
      http.Response response = await http.post(
        Uri.parse(urlsQuestion),
        headers: {"Content-Type": "application/json"},
        body: json.encode({"text": text, "quiz": quiz}),
      );
      print(response.body);
    } catch (e) {
      print(e);
    }
  }

  Future<void> addNewAnswer(String text, bool correct, int question) async {
    try {
      http.Response response = await http.post(
        Uri.parse(urlsAnswer),
        headers: {"Content-Type": "application/json"},
        body: json
            .encode({"text": text, "correct": correct, "question": question}),
      );
      print(response.body);
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteQuiz(int id) async {
    String url = "http://aplikasi-pbp.herokuapp.com/quiz/quizapi/ujian/$id/";
    try {
      http.Response response = await http.delete(Uri.parse(url));
      if (response.statusCode > 400) {
        print("Quiz not deleted");
      } else {
        print(response.body);
        print("Data");
      }
    } catch (e) {
      print("Data not deleted");
    }
  }

  Future<void> deleteQuestion(int id) async {
    String url = "http://aplikasi-pbp.herokuapp.com/quiz/quizapi/question/$id/";
    try {
      http.Response response = await http.delete(Uri.parse(url));
      if (response.statusCode > 400) {
        print("Question not deleted");
      } else {
        print(response.body);
        print("Data");
      }
    } catch (e) {
      print("Data not deleted");
    }
  }

  Future<void> deleteAnswer(int id) async {
    String url = "http://aplikasi-pbp.herokuapp.com/quiz/quizapi/answer/$id/";
    try {
      http.Response response = await http.delete(Uri.parse(url));
      if (response.statusCode > 400) {
        print("Answer not deleted");
      } else {
        print(response.body);
      }
    } catch (e) {
      print("Data not deleted");
    }
  }

  Quiz singleQuiz(int id) {
    return _quizzes.firstWhere((element) => element.id == id);
  }

  Question singleQuestion(int id) {
    return _questions.firstWhere((element) => element.id == id);
  }

  Answer singleAnswer(int id) {
    return _answers.firstWhere((element) => element.id == id);
  }

  Future<void> updateQuiz(int id, String name, String topic, int numOfQuestions,
      int time, int score) async {
    String url = "http://aplikasi-pbp.herokuapp.com/quiz/quizapi/ujian/$id/";
    try {
      http.Response response = await http.put(
        Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "name": name,
          "topic": topic,
          "number_of_questions": numOfQuestions,
          "time": time,
          "required_score_to_pass": score,
        }),
      );
      print(response.body);
      notifyListeners();
    } catch (e) {
      print("Update failed");
    }
  }

  Future<void> updateQuestion(int id, String text, int q) async {
    String url = "http://aplikasi-pbp.herokuapp.com/quiz/quizapi/question/$id/";
    try {
      http.Response response = await http.put(
        Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: json.encode({"text": text, "quiz": q}),
      );
      print(response.body);
      notifyListeners();
    } catch (e) {
      print("Update failed");
    }
  }

  Future<void> updateAnswer(int id, String text, bool corr, int q) async {
    String url = "http://aplikasi-pbp.herokuapp.com/quiz/quizapi/answer/$id/";
    try {
      http.Response response = await http.put(
        Uri.parse(url),
        headers: {"Content-Type": "application/json"},
        body: json.encode({"text": text, "correct": corr, "question": q}),
      );
      print(response.body);
      notifyListeners();
    } catch (e) {
      print("Update failed");
    }
  }
}
