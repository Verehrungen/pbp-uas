//https://app.quicktype.io/
//
//To parse this JSON data, do
//
//     final user = userFromMap(jsonString);

import 'dart:convert';

List<User> userFromMap(String str) =>
    List<User>.from(json.decode(str).map((x) => User.fromMap(x)));

String userToMap(List<User> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class User {
  User({
    this.model,
    this.pk,
    this.fields,
  });

  Model? model;
  int? pk;
  UsrFields? fields;

  factory User.fromMap(Map<String, dynamic> json) => User(
        model: modelValues.map![json["model"]],
        pk: json["pk"],
        fields: UsrFields.fromMap(json["fields"]),
      );

  Map<String, dynamic> toMap() => {
        "model": modelValues.reverse[model],
        "pk": pk,
        "fields": fields!.toMap(),
      };
}

class UsrFields {
  UsrFields({
    this.password,
    this.lastLogin,
    this.isSuperuser,
    this.username,
    this.email,
    this.isStaff,
    this.isActive,
    this.dateJoined,
    this.isAdmin,
    this.isPengajar,
    this.isMurid,
    this.firstName,
    this.lastName,
    this.city,
    this.zipCode,
    this.hobby,
    this.motto,
    this.groups,
    this.userPermissions,
  });

  String? password;
  String? lastLogin;
  bool? isSuperuser;
  String? username;
  String? email;
  bool? isStaff;
  bool? isActive;
  String? dateJoined;
  bool? isAdmin;
  bool? isPengajar;
  bool? isMurid;
  String? firstName;
  String? lastName;
  String? city;
  int? zipCode;
  String? hobby;
  String? motto;
  List<dynamic>? groups;
  List<dynamic>? userPermissions;

  factory UsrFields.fromMap(Map<String, dynamic> json) => UsrFields(
        password: json["password"],
        lastLogin: json["last_login"],
        isSuperuser: json["is_superuser"],
        username: json["username"],
        email: json["email"],
        isStaff: json["is_staff"],
        isActive: json["is_active"],
        dateJoined: json["date_joined"],
        isAdmin: json["is_admin"],
        isPengajar: json["is_pengajar"],
        isMurid: json["is_murid"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        city: json["city"],
        zipCode: json["zip_code"],
        hobby: json["hobby"],
        motto: json["motto"],
        groups: List<dynamic>.from(json["groups"].map((x) => x)),
        userPermissions:
            List<dynamic>.from(json["user_permissions"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "password": password,
        "last_login": lastLogin,
        "is_superuser": isSuperuser,
        "username": username,
        "email": emailValues.reverse[email],
        "is_staff": isStaff,
        "is_active": isActive,
        "date_joined": lastLogin,
        "is_admin": isAdmin,
        "is_pengajar": isPengajar,
        "is_murid": isMurid,
        "first_name": firstName,
        "last_name": lastName,
        "city": city,
        "zip_code": zipCode,
        "hobby": hobbyValues.reverse[hobby],
        "motto": motto,
        "groups": List<dynamic>.from(groups!.map((x) => x)),
        "user_permissions": List<dynamic>.from(userPermissions!.map((x) => x)),
      };
}

enum Email { EMPTY, WEGA_GMAIL_COM }

final emailValues =
    EnumValues({"": Email.EMPTY, "wega@gmail.com": Email.WEGA_GMAIL_COM});

enum Hobby { EMPTY, MAKAN_NASI, NGAJAR }

final hobbyValues = EnumValues(
    {"": Hobby.EMPTY, "makan nasi": Hobby.MAKAN_NASI, "ngajar": Hobby.NGAJAR});

enum Model { MAIN_USER }

final modelValues = EnumValues({"main.user": Model.MAIN_USER});

class EnumValues<T> {
  Map<String, T>? map;
  Map<T, String>? reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map!.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap!;
  }
}
