import 'dart:convert';

List<Reply> replyFromJson(String str) =>
    List<Reply>.from(json.decode(str).map((x) => Reply.fromJson(x)));

String replyToJson(List<Reply> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Reply {
  Reply({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  factory Reply.fromJson(Map<String, dynamic> json) => Reply(
        model: json["model"]!,
        pk: json["pk"]!,
        fields: json["fields"] == null ? null : Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model!,
        "pk": pk!,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.author,
    this.post,
    this.content,
    this.timestamp,
  });

  int? author;
  int? post;
  String? content;
  DateTime? timestamp;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        author: json["author"]!,
        post: json["post"]!,
        content: json["content"]!,
        timestamp: DateTime.parse(json["timestamp"]!),
      );

  Map<String, dynamic> toJson() => {
        "author": author!,
        "post": post!,
        "content": content!,
        "timestamp": timestamp?.toIso8601String(),
      };
}
