// To parse this JSON data, do
//
//     final contentFile = contentFileFromJson(jsonString);

import 'dart:convert';

List<ContentFile> contentFileFromJson(String str) => List<ContentFile>.from(
    json.decode(str).map((x) => ContentFile.fromJson(x)));

String contentFileToJson(List<ContentFile> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ContentFile {
  ContentFile({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  factory ContentFile.fromJson(Map<String, dynamic> json) => ContentFile(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.course,
    this.title,
    this.file,
  });

  int? course;
  String? title;
  String? file;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        course: json["course"],
        title: json["title"],
        file: json["file"],
      );

  Map<String, dynamic> toJson() => {
        "course": course,
        "title": title,
        "file": file,
      };
}
