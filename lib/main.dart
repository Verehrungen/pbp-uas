// import 'package:flutter/gestures.dart';
import 'package:tkpbp/api/api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tkpbp/models/global_state.dart';
import 'package:tkpbp/pages/auth/login.dart';
import 'package:tkpbp/pages/auth/signup.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tkpbp/models/user.dart';
import 'dart:convert';

import 'package:tkpbp/pages/home.dart';

// import 'package:flutter/services.dart';
// import 'package:url_launcher/url_launcher.dart';
void main() => runApp(const MyApp());

const String _title = 'Learning App';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<User>? loggedInUser;

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      getLoggedInUser();
    });
    super.initState();
  }

  void getLoggedInUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("loggedUser")) {
      var myData = json.decode(localData.getString("loggedUser")!)
          as Map<String, dynamic>;
      loggedInUser = myData['current_user'];
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => GState(),
      child: MaterialApp(
        title: 'Home Page',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.purple, primaryColor: Colors.purple),
        home: loggedInUser == null
            ? HomePage(
                title: 'HOME PAGE',
              )
            : HomeLab7(loggedInUser: loggedInUser),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 60),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  const Text(
                    "Selamat Datang",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                  const Text(
                    "di Learning App",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Platform Belajar Terintegrasi Gratis",
                    style: TextStyle(
                      color: Colors.grey[800],
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    "Kelas Online Berkualitas",
                    style: TextStyle(
                      color: Colors.grey[800],
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(height: 50),
                  Container(
                    height: MediaQuery.of(context).size.height / 3,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/welcome.png"),
                      ),
                    ),
                  ),
                  const SizedBox(height: 50),
                  Column(children: <Widget>[
                    MaterialButton(
                      minWidth: double.infinity,
                      height: 60,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => LoginPage()),
                        );
                      },
                      shape: RoundedRectangleBorder(
                          side: const BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(50)),
                      child: const Text(
                        "Login",
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                    ),
                    const SizedBox(height: 20),
                    MaterialButton(
                      minWidth: double.infinity,
                      height: 60,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SignUpPage()),
                        );
                      },
                      color: Colors.purple[400],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      child: const Text(
                        "Sign Up",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 18),
                      ),
                    ),
                  ]),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// /// This is the stateful widget that the main application instantiates.
// class MyStatefulWidget extends StatefulWidget {
//   const MyStatefulWidget({Key? key}) : super(key: key);

//   @override
//   State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
// }

// /// This is the private State class that goes with MyStatefulWidget.
// class _MyStatefulWidgetState extends State<MyStatefulWidget> {
//   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
//   static TextEditingController controller1 = TextEditingController();
//   static TextEditingController controller2 = TextEditingController();
//   List<String> array1 = ["Google", "Facebook", "Twitter"];
//   List<String> array2 = [
//     "https://www.google.com",
//     "https://www.facebook.com",
//     "https://www.twitter.com"
//   ];

//   // @override
//   // void dispose() {
//   //   // Clean up the controller when the widget is disposed.
//   //   controller1.dispose();
//   //   controller2.dispose();
//   //   super.dispose();
//   // }

//   clearTextInput() {
//     controller1.clear();
//     controller2.clear();
//   }

//   _getRequests() async {}

//   getArray1() {
//     return array1;
//   }

//   getArray2() {
//     return array2;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Form(
//       key: _formKey,
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//         children: <Widget>[
//           Text('Add Content',
//               style: TextStyle(
//                 fontSize: 34,
//                 height: 2.0,
//               )),
//           Text('This field is required.',
//               style: TextStyle(
//                 fontSize: 14,
//                 height: 2.0,
//               )),
//           Text('Title:',
//               style: TextStyle(
//                 fontSize: 18,
//                 height: 2.0,
//               )),
//           TextFormField(
//             controller: controller1,
//             validator: (String? value) {
//               if (value == null || value.isEmpty) {
//                 return 'Fill out this field';
//               }
//               return null;
//             },
//           ),
//           Text('This field is required.',
//               style: TextStyle(
//                 fontSize: 14,
//                 height: 2.0,
//               )),
//           Text('File:',
//               style: TextStyle(
//                 fontSize: 18,
//                 height: 2.0,
//               )),
//           TextFormField(
//             controller: controller2,
//             validator: (String? value) {
//               if (value == null || value.isEmpty) {
//                 return 'Fill out this field';
//               }
//               return null;
//             },
//           ),
//           Padding(
//             padding: const EdgeInsets.all(16),
//             child: ElevatedButton(
//               onPressed: () {
//                 if (_formKey.currentState!.validate()) {
//                   showDialog<String>(
//                     context: context,
//                     builder: (BuildContext context) => AlertDialog(
//                       title: Row(children: <Widget>[
//                         new RichText(
//                           text: new TextSpan(
//                             children: [
//                               new TextSpan(
//                                 text: controller1.text,
//                                 style: new TextStyle(color: Colors.blue),
//                                 recognizer: new TapGestureRecognizer()
//                                   ..onTap = () {
//                                     launch(controller2.text);
//                                   },
//                               ),
//                             ],
//                           ),
//                         ),
//                       ]),
//                     ),
//                   );
//                   Future.delayed(
//                       const Duration(seconds: 2),
//                       () => Navigator.of(context)
//                           .push(
//                             new MaterialPageRoute(
//                                 builder: (_) => new DestinationScreen()),
//                           )
//                           .then((val) => val ? _getRequests() : null));
//                   // clearTextInput();
//                 }
//               },
//               child: const Text('SUBMIT'),
//               style: ElevatedButton.styleFrom(primary: Colors.green),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

// class DestinationScreen extends StatefulWidget {
//   DestinationScreen({Key? key}) : super(key: key);

//   @override
//   _DestinationScreenState createState() => _DestinationScreenState();
// }

// class _DestinationScreenState extends State<DestinationScreen> {
//   static const String _title = 'Learning App';
//   final statefulwidg = _MyStatefulWidgetState();

//   bool _isVisible = true;

//   void showToast() {
//     setState(() {
//       _isVisible = !_isVisible;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: Icon(Icons.toc),
//         title: const Text(_title),
//         centerTitle: true,
//         backgroundColor: Colors.purple,
//       ),
//       body: Column(
//         children: [
//           Padding(
//             padding: const EdgeInsets.all(16),
//             child: Align(
//               alignment: Alignment.centerLeft,
//               child: ElevatedButton(
//                 onPressed: () {
//                   Navigator.pop(context, true);
//                 },
//                 child: const Text('+'),
//                 style: ElevatedButton.styleFrom(
//                     primary: Colors.green, shape: CircleBorder()),
//               ),
//             ),
//           ),
//           for (var i = 0; i < 3; i++)
//             Visibility(
//               visible: _isVisible,
//               child: Card(
//                   shape: RoundedRectangleBorder(
//                     side: BorderSide(
//                       color: Colors.grey.withOpacity(0.2),
//                       width: 1,
//                     ),
//                   ),
//                   child: Container(
//                       width: 400,
//                       height: 50,
//                       child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             new RichText(
//                               text: new TextSpan(
//                                 children: [
//                                   new TextSpan(
//                                     text: statefulwidg.getArray1()[i],
//                                     style: new TextStyle(color: Colors.blue),
//                                     recognizer: new TapGestureRecognizer()
//                                       ..onTap = () {
//                                         launch(statefulwidg.getArray2()[i]);
//                                       },
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             ElevatedButton(
//                               onPressed: showToast,
//                               child: const Text('DELETE'),
//                               style:
//                                   ElevatedButton.styleFrom(primary: Colors.red),
//                             )
//                           ]))),
//             ),
//         ],
//       ),
//     );
//   }
// }
