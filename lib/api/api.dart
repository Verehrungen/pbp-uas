import 'dart:convert';
import 'package:flutter/material.dart';
import '../models/content.dart';
import 'package:http/http.dart' as http;

class ContentProvider with ChangeNotifier {
  ContentProvider() {
    this.fetchContent();
  }
  List<Content> _content = [];

  List<Content> get content {
    return [..._content];
  }

  fetchContent() async {
    // ignore: prefer_const_declarations
    final url = 'http://localhost:8000/content/json/?format=json';
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      var data = json.decode(response.body) as List;
      _content = data.map<Content>((json) => Content.fromJson(json)).toList();
    }
  }
}
