import 'package:flutter/material.dart';
import 'package:tkpbp/main.dart';
import 'package:tkpbp/pages/course/allcourses.dart';
import 'package:tkpbp/pages/helpdesk/contact_us.dart';
import 'package:tkpbp/pages/helpdesk/home_helpdesk.dart';
import 'package:tkpbp/pages/profile/show/welcome_screen.dart';
import 'package:tkpbp/pages/contents/content.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tkpbp/models/user.dart';

import 'dart:convert';

import 'package:tkpbp/pages/quiz/lecturer_quiz_screen.dart';

class MainDrawer extends StatelessWidget {
  List<User>? loggedInUser;
  MainDrawer({
    Key? key,
    required this.loggedInUser,
  }) : super(key: key);

  void getLoggedInUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("loggedUser")) {
      var myData = json.decode(localData.getString("loggedUser")!)
          as Map<String, dynamic>;
      loggedInUser = myData['current_user'];
    }
  }

  void removeLoggedInUser() async {
    var localData = await SharedPreferences.getInstance();
    if (localData.containsKey("loggedUser")) {
      localData.remove("loggedUser");
    }
  }

  Widget buildListTile(String title, IconData icon) {
    return ListTile(
      leading: Icon(
        icon,
        size: 24,
      ),
      title: Text(
        title,
        style: TextStyle(
          // fontFamily: 'RobotoCondensed',
          fontSize: 16,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: () {},
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            // height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            color: Color(0xffd500f9),
            child: Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Welcome,',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                      color: Colors.white),
                ),
                Text(
                  '${loggedInUser![0].fields!.username!}',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 24,
                      color: Colors.white),
                ),
                Text(
                  '${loggedInUser![0].fields!.firstName!}' +
                      " " +
                      '${loggedInUser![0].fields!.lastName!}',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                      color: Colors.white),
                ),
                loggedInUser![0].fields!.isMurid!
                    ? Text(
                        'Murid',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.white),
                      )
                    : loggedInUser![0].fields!.isAdmin!
                        ? Text(
                            'Admin',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: Colors.white),
                          )
                        : Text(
                            'Guru/Dosen',
                            style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 15,
                                color: Colors.white),
                          )
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => WelcomeScreen(
                            loggedIn: loggedInUser,
                          )));
            },
            icon: Icon(
              Icons.account_box,
              // color: color,
            ),
            label: Text("Profile"),
            style: ElevatedButton.styleFrom(
              primary: Colors.purple[900]!,
              minimumSize: Size(240, 40),
            ),
          ),
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        AllCourses(loggedInUser: loggedInUser),
                  ));
            },
            icon: Icon(
              Icons.book,
              // color: color,
            ),
            label: Text("All Courses"),
            style: ElevatedButton.styleFrom(
              primary: Colors.purple[900]!,
              minimumSize: Size(240, 40),
            ),
          ),
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LecturerQuizScreen(),
                  ));
            },
            icon: Icon(
              Icons.task_alt,
              // color: color,
            ),
            label: Text("My Quiz"),
            style: ElevatedButton.styleFrom(
              primary: Colors.purple[800]!,
              minimumSize: Size(240, 40),
            ),
          ),
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ContentWidget(),
                  ));
            },
            icon: Icon(
              Icons.edit,
              // color: color,
            ),
            label: Text("Add Files"),
            style: ElevatedButton.styleFrom(
              primary: Colors.purple[700]!,
              minimumSize: Size(240, 40),
            ),
          ),
          ElevatedButton.icon(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DestinationScreen(),
                  ));
            },
            icon: Icon(
              Icons.description,
              // color: color,
            ),
            label: Text("My Files"),
            style: ElevatedButton.styleFrom(
              primary: Colors.purple[800]!,
              minimumSize: Size(240, 40),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          ListTile(
            leading: Icon(
              Icons.chat,
              size: 24,
            ),
            title: Text(
              'Need Help ?',
              style: TextStyle(
                // fontFamily: 'RobotoCondensed',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => HomeHelpdesk(),
                  ));
            },
          ),
          ListTile(
            leading: Icon(
              Icons.phone,
              size: 24,
            ),
            title: Text(
              'Contacts',
              style: TextStyle(
                // fontFamily: 'RobotoCondensed',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ContactUs(),
                  ));
            },
          ),
          Expanded(
              child: Align(
            alignment: Alignment.bottomCenter,
            child: ElevatedButton.icon(
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomePage(title: "HOME PAGE"),
                    ));
              },
              icon: Icon(
                Icons.logout,
                // color: color,
              ),
              label: Text("Logout"),
              style: ElevatedButton.styleFrom(
                primary: Colors.red.withOpacity(0.8),
                minimumSize: Size(240, 40),
              ),
            ),
          ))
        ],
      ),
    );
  }
}
