import 'package:flutter/material.dart';
import 'package:tkpbp/widgets/components/text_field_container.dart';
import 'package:tkpbp/constants.dart';

class RoundedInputField extends StatelessWidget {
  final String? hintText;
  final IconData? icon;
  final ValueChanged<String>? onChanged;
  const RoundedInputField({
    Key? key,
    this.hintText,
    this.icon,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(
            icon,
            color: kPrimaryColor,
          ),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}

class RoundedInputFieldDisabled extends StatefulWidget {
  final String? hintText;
  final String? data;
  final IconData? icon;
  final ValueChanged<String>? onChanged;
  RoundedInputFieldDisabled(
      {Key? key,
      required this.hintText,
      this.icon,
      this.onChanged,
      required this.data})
      : super(key: key);

  @override
  State<RoundedInputFieldDisabled> createState() =>
      _RoundedInputFieldDisabledState();
}

class _RoundedInputFieldDisabledState extends State<RoundedInputFieldDisabled> {
  var contrl = TextEditingController();
  @override
  void initState() {
    // TODO: implement initState
    contrl.text = widget.data ?? "Kosong";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        controller: contrl,
        enabled: false,
        onChanged: widget.onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(
            widget.icon,
            color: kPrimaryColor,
          ),
          labelText: widget.hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
